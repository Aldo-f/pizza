<?php

/**
 * Funtioneel
 */
require "src/functions/require.php";
require "vendor/autoload.php";
require "src/twig.php";
require "src/functions/ingelogd.php";
if (isset($_POST)) {
    require "src/functions/loginAccount.php";
}

if ($ingelogd) {
    // Toon melding dat je reeds ingelod bent bij loginForm.twig

    $voornaam = $account->getVoornaam();
    // $msgSuccess["header"] = "$voornaam, je bent reeds ingelogd";
    array_push($msgSuccess["msg"], "<a href='index.php'>Bestel</a> je pizza");
}


/**
 * Visueel
 */

require "src/presentation/layout_parts/header.php";


if (isset($_COOKIE['email'])) {
    $email =  $_COOKIE['email'];
    $view = $twig->render(
        "loginForm.twig",
        array("email" => $email)
    );
} else {
    $view = $twig->render(
        "loginForm.twig"
    );
}
print($view);


require "src/presentation/layout_parts/footer.php";