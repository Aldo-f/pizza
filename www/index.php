<?php
use pizza\business\bedrijf\LeverLijnService;
use pizza\functions\helpers\WinkelmandProducten;
use pizza\business\bedrijf\BedrijfService;

/**
 * Funtioneel
 */
require "src/functions/require.php";
require "vendor/autoload.php";
require "src/twig.php";
require "src/functions/ingelogd.php";
require "src/functions/winkelmand.php";

if (!isset($postcode)) {
    $postcode = $winkelmand->sessie->postcode;
}

$_ENV;

if (isset($postcode)) {

    // maak een keuzelijst voor bedrijf
    $leverLijnSvc = new LeverLijnService;
    $leverLijnLijst = $leverLijnSvc->getByPostcode($postcode);
    if ($leverLijnLijst) {

        $idBedrijfLijst = [];
        foreach ($leverLijnLijst as $key) {
            array_push($idBedrijfLijst, $key->getIdBedrijf());
        }

        // maak $idBedrijfLijst met enkel unique value's
        $idBedrijfLijst = array_unique($idBedrijfLijst);

        // haal namen op van de bedrijven
        $bedrijfSvc = new BedrijfService;
        $keuzeBedrijfLijst = [];
        foreach ($idBedrijfLijst as $key => $idBedrijf) {
            $bedrijf = $bedrijfSvc->getById($idBedrijf);
            array_push($keuzeBedrijfLijst, $bedrijf);
        }

        // maak bedrijfId
        if (!isset($bedrijfId)) {
            $bedrijfId = $winkelmand->sessie->bedrijfId;
        }

        // Haal producten op
        if (isset($bedrijfId)) {
            $winkelSvc = new WinkelmandProducten;
            $productLijst = $winkelSvc->maakProductLijstBeschikbaar($bedrijfId);
        }
    } else {
        $geenBedrijfVoorPostcode = true;
        // geen bedrijf voor de gekoze postcode

        $msgError["header"] = "Geen bedrijf gevonden";
        array_push($msgError["msg"], "Voor je huidige postcode ($postcode) zijn er nog geen bedrijven die leveren.");
    }
}



//maak productlijst van winkelmandje
if (isset($winkelmand)) {
    $gekozenBedrijf = $winkelmand->sessie->bedrijfId;
    // het huidig geselcteerde bedrijf is deze van de sessie
    if (isset($gekozenBedrijf) && isset($bedrijfId) && $bedrijfId == $gekozenBedrijf) {
        $winkelSvc = new WinkelmandProducten;

        $mandProductLijst = [];
        $idProductLijst = $winkelmand->sessie->producten;
        if (sizeof($idProductLijst) > 0) {
            foreach ($idProductLijst as $key => $idProduct) {
                // echo "key:" . $key . ", idProduct: " . $idProduct;
                $product = $winkelSvc->maakProduct($idProduct);
                array_push($mandProductLijst, $product);
            }
        }
    }
}


// Fix voor geen undefined vars door te sturen 
require "src/functions/maakVars.php";

/**
 * Visueel
 */
require "src/presentation/layout_parts/header.php";


$view = $twig->render(
    "overzicht.twig",
    array(
        "account" => $account,
        "postcode" => $postcode,
        "bedrijfId" => $bedrijfId,
        "winkelmand" => $winkelmand,
        "productLijst" => $productLijst,
        "mandProductLijst" => $mandProductLijst,
        "idBedrijfLijst" => $idBedrijfLijst,
        "geenBedrijfVoorPostcode" => $geenBedrijfVoorPostcode,
        "keuzeBedrijfLijst" => $keuzeBedrijfLijst,

        // debug
        "geenWaarde" => $geenWaarde
    )
);
print($view);

require "src/presentation/layout_parts/footer.php";