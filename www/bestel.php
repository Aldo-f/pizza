<?php
use pizza\functions\helpers\WinkelmandProducten;

/**
 * Funtioneel
 */
require "src/functions/require.php";
require "vendor/autoload.php";
require "src/twig.php";
require "src/functions/ingelogd.php";
require "src/functions/winkelmand.php";
require "src/functions/getAccount.php";
if (isset($_POST) && sizeof($_POST) > 0) {

    if (isset($_POST['actie'])) {
        if ($_POST["actie"] = "registreerAccountBestel") {
            require "src/functions/registreerAccountBestel.php";
        }
        if ($_POST["actie"] = "plaatsBestelling") {
            require "src/functions/setBestelling.php";
        }
        if ($_POST["actie"] = "loginAccount") {
            require "src/functions/loginAccount.php";
            require "src/functions/getAccount.php";
        }
    }
}
if (!isset($postcode)) {
    $postcode = $winkelmand->sessie->postcode;
}


//maak productlijst van winkelmandje
if (isset($winkelmand)) {
    $gekozenBedrijf = $winkelmand->sessie->bedrijfId;
    // het huidig geselcteerde bedrijf is deze van de sessie
    // if (isset($gekozenBedrijf) && $bedrijfId == $gekozenBedrijf) {
    $winkelSvc = new WinkelmandProducten;

    $mandProductLijst = [];
    foreach ($winkelmand->sessie->producten as $key => $idProduct) {
        // echo "key:" . $key . ", idProduct: " . $idProduct;
        $product = $winkelSvc->maakProduct($idProduct);
        array_push($mandProductLijst, $product);
    }
    // }
}

$minLevertijd =  strftime('%Y-%m-%dT%H:%M:%S', strtotime("+45 minutes"));
$defaultLevertijd = strftime('%Y-%m-%dT%H:%M:%S', strtotime("+45 minutes"));
$maxLevertijd = strftime('%Y-%m-%dT%H:%M:%S', strtotime("+ 1 week"));


// Fix voor geen undefined vars door te sturen 
require "src/functions/maakVars.php";

/**
 * Visueel
 */
require "src/presentation/layout_parts/header.php";


$view = $twig->render(
    "bestel.twig",
    array(
        "account" => $account,
        "postcode" => $postcode,
        "bedrijfId" => $bedrijfId,
        "winkelmand" => $winkelmand,
        "productLijst" => $productLijst,
        'mandProductLijst' => $mandProductLijst,
        "plaats" => $plaats,
        "minLevertijd" => $minLevertijd,
        "defaultLevertijd" => $defaultLevertijd,
        "maxLevertijd" => $maxLevertijd,

        // debug
        "geenWaarde" => $geenWaarde
    )
);
print($view);

require "src/presentation/layout_parts/footer.php";
