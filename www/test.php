<?php
use pizza\business\PlaatsService;
use pizza\business\AccountService;
use pizza\business\product\SausService;
use pizza\business\product\BodemService;
use pizza\business\bedrijf\BedrijfService;
use pizza\business\product\FormaatService;
use pizza\business\product\ProductService;
use pizza\business\bedrijf\LeverLijnService;
use pizza\business\product\IngredientService;
use pizza\business\product\ProductLijnService;
use pizza\business\bestelling\BestellingService;
use pizza\business\product\IngredientLijnService;
use pizza\business\bestelling\BestellingLijnService;

/**
 * Funtioneel
 */

require "src/functions/require.php";
require "vendor/autoload.php";
require "src/twig.php";
require "src/functions/ingelogd.php";

// $msgSuccess["header"] = "Successvol uitgelogd";
// array_push($msgSuccess["msg"], "U hebt zich successvol uitgelogd");

$test = "Een klein test tekstje";

$accountSvc = new AccountService;
$accountLijst = $accountSvc->getAll();
$accountById = $accountSvc->getById(4);
$accountByEmail = $accountSvc->getByEmail("test@getnada.com");

$plaatsSvc = new PlaatsService;
$plaatsLijst = $plaatsSvc->getAll();
$plaatsById = $plaatsSvc->getById(2);

$bestellingSvc = new BestellingService;
$bestellingLijst = $bestellingSvc->getAll();
$bestellingBasicLijst = $bestellingSvc->getBasicAll();
$bestellingById = $bestellingSvc->getById(1);
$bestellingByIdAccount = $bestellingSvc->getByIdAccount(8);

$bestellingLijnSvc = new BestellingLijnService;
$bestellingLijnLijst = $bestellingLijnSvc->getAll();
$bestellingLijnByIdPromotieLijn = $bestellingLijnSvc->getByIdPromotieLijn(1);
$bestellingLijnByProductLijn = $bestellingLijnSvc->getByIdProductLijn(2);
$bestellingLijnByIdBestelling = $bestellingLijnSvc->getByIdBestelling(1);

$bedrijfSvc = new BedrijfService;
$bedrijfLijst = $bedrijfSvc->getAll();
$bedrijfById = $bedrijfSvc->getById(2);

$leverLijnSvc = new LeverLijnService;
$leverLijnLijst = $leverLijnSvc->getAll();
$leverLijnByIdBedrijf = $leverLijnSvc->getByIdBedrijf(2);
$leverLijnByIdPlaats = $leverLijnSvc->getByIdPlaats(8);

$productLijnSvc = new ProductLijnService;
$productLijnLijst = $productLijnSvc->getAll();
$productLijnByIdBedrijf = $productLijnSvc->getByIdBedrijf(1);
$productLijnByIdProduct = $productLijnSvc->getByIdProduct(1);

$productSvc = new ProductService;
$productLijst = $productSvc->getAll();
$productById = $productSvc->getById(2);

$formaatSvc = new FormaatService;
$formaatLijst = $formaatSvc->getAll();
$formaatById = $formaatSvc->getById(3);

$bodemSvc = new BodemService;
$bodemLijst = $bodemSvc->getAll();
$bodemById = $bodemSvc->getById(2);

$sausSvc = new SausService;
$sausLijst = $sausSvc->getAll();
$sausById = $sausSvc->getById(2);

$ingredientLijnSvc = new IngredientLijnService;
$ingredientLijnLijst = $ingredientLijnSvc->getAll();
$ingredientLijnByIdIngredient = $ingredientLijnSvc->getByIdIngredient(22);
$ingredientLijnByIdProduct = $ingredientLijnSvc->getByIdProduct(3);

$ingredientSvc = new IngredientService;
$ingredientLijst = $ingredientSvc->getAll();
$ingredientById = $ingredientSvc->getById(4);

/// Combineer
// Prijs van 1 pizza
$mijnProductId = 2;
$mijnProduct = $productSvc->getProductById($mijnProductId);


/**
 * Visueel
 */
require "src/presentation/layout_parts/header.php";

$view = $twig->render(
    "test.twig",
    array(
        "test" => $test,

        "accountLijst" => $accountLijst,
        "accountById" => $accountById,
        "accountByEmail" => $accountByEmail,

        "plaatsLijst" => $plaatsLijst,
        "plaatsById" => $plaatsById,

        "bestellingLijst" => $bestellingLijst,
        "bestellingBasicLijst" => $bestellingBasicLijst,
        "bestellingById" => $bestellingById,
        "bestellingByIdAccount" => $bestellingByIdAccount,

        "bestellingLijnLijst" => $bestellingLijnLijst,
        "bestellingLijnByIdPromotieLijn" => $bestellingLijnByIdPromotieLijn,
        "bestellingLijnByProductLijn" => $bestellingLijnByProductLijn,
        "bestellingLijnByIdBestelling" => $bestellingLijnByIdBestelling,

        "bedrijfLijst" => $bedrijfLijst,
        "bedrijfById" => $bedrijfById,

        "leverLijnLijst" => $leverLijnLijst,
        "leverLijnByIdBedrijf" => $leverLijnByIdBedrijf,
        "leverLijnByIdPlaats" => $leverLijnByIdPlaats,

        "productLijnLijst" => $productLijnLijst,
        "productLijnByIdBedrijf" => $productLijnByIdBedrijf,
        "productLijnByIdProduct" => $productLijnByIdProduct,

        "productLijst" => $productLijst,
        "productById" => $productById,

        "formaatLijst" => $formaatLijst,
        "formaatById" => $formaatById,

        "bodemLijst" => $bodemLijst,
        "bodemById" => $bodemById,

        "sausLijst" => $sausLijst,
        "sausById" => $sausById,

        "ingredientLijnLijst" => $ingredientLijnLijst,
        "ingredientLijnByIdIngredient" => $ingredientLijnByIdIngredient,
        "ingredientLijnByIdProduct" => $ingredientLijnByIdProduct,

        "ingredientLijst" => $ingredientLijst,
        "ingredientById" => $ingredientById,

        "mijnProduct" => $mijnProduct
    )
);
print($view);

require "src/presentation/layout_parts/footer.php";