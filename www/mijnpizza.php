<?php
use pizza\functions\helpers\WinkelmandProducten;

/**
 * Funtioneel
 */
require "src/functions/require.php";
require "vendor/autoload.php";
require "src/twig.php";
require "src/functions/ingelogd.php";
require "src/functions/updateProduct.php"; // geeft eventueel $vervangProduct door aan winkelmand.php

require "src/functions/winkelmand.php";







// Fix voor geen undefined vars door te sturen 
require "src/functions/maakVars.php";

/**
 * Visueel
 */
require "src/presentation/layout_parts/header.php";


$view = $twig->render(
    "productForm.twig",
    array(
        "account" => $account,
        "winkelmand" => $winkelmand,
        "idProduct" => $idProduct,
        "bedrijfId" => $bedrijfId,
        "product" => $product,

        "formaatLijst" => $formaatLijst,
        'bodemLijst' => $bodemLijst,
        "sausLijst" => $sausLijst,
        // "ingredientLijst" => $ingredientLijst,

        // debug
        "geenWaarde" => $geenWaarde
    )
);
print($view);

require "src/presentation/layout_parts/footer.php";
