var gulp = require('gulp'),
    less = require('gulp-less'),
    mini = require('gulp-cssnano'),
    ugly = require('gulp-uglify');


//style paths
var lessFiles = [
        'src/assets/less/style.less'
    ],
    cssDest = 'src/assets/css/',
    cssMiniDest = 'src/assets/min.css/';

var jsFiles = [
        'src/assets/js-mijn/script.js'
    ],
    jsDest = 'src/assets/js/',
    jsMiniDest = 'src/assets/min.js/';

const {
    series
} = require('gulp');

const {
    parallel
} = require('gulp');

// The `clean` function is not exported so it can be considered a private task.
// It can still be used within the `series()` composition.
function clean(cb) {
    // body omitted
    cb();
}

// The `build` function is exported so it is public and can be run with the `gulp` command.
// It can also be used within the `series()` composition.
function build(cb) {
    // body omitted
    cb();
}

function watch(cb) {
    gulp.watch(lessFiles, css);
    gulp.watch(jsFiles, javascript);
}

/**
 * Maak js files
 */
function javascript(cb) {
    // body omitted
    cb();
    gulp.src(jsFiles)
        .pipe(ugly()) // min and normal
        .pipe(gulp.dest(jsDest));
}

/**
 * Maak css files
 */
function css(cb) {
    // body omitted
    cb();
    gulp.src(lessFiles)
        .pipe(less()) // maak css
        .pipe(gulp.dest(cssDest))
        .pipe(mini()) // minimize
        .pipe(gulp.dest(cssMiniDest));
}


// Start gulp functions 
exports.build = series(clean, parallel(css, javascript));
exports.default = series(clean, build);
exports.watch = series(watch);