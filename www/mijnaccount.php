<?php

/**
 * Funtioneel
 */
require "src/functions/require.php";
require "vendor/autoload.php";
require "src/twig.php";
require "src/functions/ingelogd.php";
if (isset($_POST["actie"]) && $_POST["actie"] == "updateAccount") {
    require "src/functions/updateAccount.php";
}
require "src/functions/getAccount.php";





// Fix voor geen undefined vars door te sturen 
require "src/functions/maakVars.php";

/**
 * Visueel
 */
require "src/presentation/layout_parts/header.php";


$view = $twig->render(
    "account.twig",
    array(
        "account" => $account,
        "plaats" => $plaats,

        // debug
        "geenWaarde" => $geenWaarde
    )
);
print($view);

require "src/presentation/layout_parts/footer.php";