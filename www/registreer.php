<?php

/**
 * Funtioneel
 */
require "src/functions/require.php";
require "vendor/autoload.php";
require "src/twig.php";
require "src/functions/ingelogd.php";
if (isset($_POST)) {
    require "src/functions/registreerAccount.php";
}

// $msgSuccess["header"] = "Test";
// array_push($msgSuccess["msg"], "Registreer");


/**
 * Visueel
 */
require "src/presentation/layout_parts/header.php";

$view = $twig->render(
    "registreerForm.twig"
    // array("geldladeLijst" => $geldladeLijst, "ingelogd" => $ingelogd)
);
print($view);


require "src/presentation/layout_parts/footer.php";