<?php

use pizza\business\bestelling\BestellingService;
use pizza\business\bestelling\BestellingLijnService;

/**
 * Funtioneel
 */
require "src/functions/require.php";
require "vendor/autoload.php";
require "src/twig.php";
require "src/functions/ingelogd.php";


//maak productlijst van winkelmandje
if (isset($account)) {


    $idAccount = $account->getIdAccount();

    //    echo $accountId;
    if ($idAccount) {
        $bestellingSvc = new BestellingService;
        // $bestellingLijst = $bestellingSvc->getBasicByIdAccount($idAccount);
        $bestellingLijst = $bestellingSvc->getByIdAccount($idAccount);

        // haal meer info op per 
        $bestellingLijnSvc = new BestellingLijnService;
        $bestellingLijnLijst = [];
        foreach ($bestellingLijst as $key) {
            $idBestelling =  $key->getIdBestelling();

            $bestellingLijn = $bestellingLijnSvc->getByIdBestelling($idBestelling); 
            array_push($bestellingLijnLijst, $bestellingLijn);
        }
     }

    
    
}


// Fix voor geen undefined vars door te sturen 
require "src/functions/maakVars.php";

/**
 * Visueel
 */
require "src/presentation/layout_parts/header.php";


$view = $twig->render(
    "overzichtBestellingen.twig",
    array(
        "account" => $account,
        "bestellingLijst" => $bestellingLijst,
        "bestellingLijnLijst" => $bestellingLijnLijst,

        // debug
        "geenWaarde" => $geenWaarde
    )
);
print($view);

require "src/presentation/layout_parts/footer.php";
