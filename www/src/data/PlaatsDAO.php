<?php
namespace pizza\data;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\Plaats;

class PlaatsDAO
{
    private function firstLetterUp($text)
    {
        return ucwords(strtolower($text));
    }

    public function getAll()
    {
        $sql =
            "SELECT idPlaats, postcode, plaats
            FROM aldofi1q_opleiding_pizza.plaats";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = Plaats::create(
                $rij["idPlaats"],
                $rij["postcode"],
                $rij['plaats']
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getById($id)
    {
        $sql =
            "SELECT idPlaats, postcode, plaats
            FROM aldofi1q_opleiding_pizza.plaats
            WHERE idPlaats = :id;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$rij) {
            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $item = Plaats::create(
                $rij["idPlaats"],
                $rij["postcode"],
                $rij['plaats']
            );
            $dbh = null;
            return $item;
        }
    }

    public function getBy($postcode, $plaats)
    {
        // Pas plaats aan
        $plaats = self::firstLetterUp($plaats);

        $sql =
            "SELECT idPlaats, postcode, plaats
            FROM aldofi1q_opleiding_pizza.plaats
            WHERE postcode = :postcode AND plaats = :plaats";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':postcode' => $postcode,
            ':plaats' => $plaats
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$rij) {
            return null;
            // TODO: exception omdat combinatie postcode en plaats niet bestaat
        } else {
            $item = Plaats::create(
                $rij["idPlaats"],
                $rij["postcode"],
                $rij['plaats']
            );
            $dbh = null;
            return $item;
        }
    }

    public function getByPlaats($plaats)
    {
        // Pas plaats aan
        $plaats = self::firstLetterUp($plaats);

        $sql =
            "SELECT idPlaats, postcode, plaats
            FROM aldofi1q_opleiding_pizza.plaats
            WHERE plaats = :plaats";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':plaats' => $plaats
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$rij) {
            return null;
            // TODO: exception omdat plaats niet bestaat
        } else {
            $item = Plaats::create(
                $rij["idPlaats"],
                $rij["postcode"],
                $rij['plaats']
            );
            $dbh = null;
            return $item;
        }
    }

    public function create($postcode, $plaats)
    {
        // Doe hier controle van postcode en plaats
        $bestaat = self::getBy($postcode, $plaats);

        if (!$bestaat) {
            // pas inputs aan
            $plaats = self::firstLetterUp($plaats);

            $dbh = new PDO(
                DBconfig::$DB_CONNSTRING,
                DBconfig::$DB_USERNAME,
                DBconfig::$DB_PASSWORD
            );

            $sql =
                "INSERT INTO `aldofi1q_opleiding_pizza`.`plaats` (`postcode`, `plaats`) 
                VALUES (:postcode, :plaats)";


            $stmt = $dbh->prepare($sql);
            $stmt->execute(array(
                ':postcode' => $postcode,
                ':plaats' => $plaats
            ));

            $laatsteId = $dbh->lastInsertId();

            if ($laatsteId > 0) {
                /* Update worked because query affected X amount of rows. */
                $result = $laatsteId;
            } else {
                $error = $dbh->errorInfo(); // Toont de DB-error
                $result = false;
                // TODO: exception omwille van fout bij creatie plaats 
            }

            // Verbreek connectie
            $dbh = null;

            // Return msg
            return $result;
        }
    }
}
