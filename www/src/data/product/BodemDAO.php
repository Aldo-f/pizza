<?php
namespace pizza\data\product;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\product\Bodem;

class BodemDAO
{
    // getAll
    // getById

    public function getAll()
    {
        $sql =
            "SELECT idBodem, idSamenstelling, prijs, naam
            FROM aldofi1q_opleiding_pizza.bodem;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = Bodem::create(
                $rij["idBodem"],
                $rij["idSamenstelling"],
                $rij["prijs"],
                $rij["naam"]
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getById($id)
    {
        $sql =
            "SELECT idBodem, idSamenstelling, prijs, naam 
            FROM aldofi1q_opleiding_pizza.bodem
            WHERE idBodem = :id";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$rij) {
            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $item = Bodem::create(
                $rij["idBodem"],
                $rij["idSamenstelling"],
                $rij["prijs"],
                $rij["naam"]
            );
            return $item;
        }
    }

    public function create($idSamenstelling, $naam, $prijs)
    {

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "INSERT INTO `aldofi1q_opleiding_pizza`.`bodem` (`idSamenstelling`, `naam`, `prijs`) 
            VALUES (:idSamenstelling, :naam, :prijs)";


        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idSamenstelling' => $idSamenstelling,
            ':naam' => $naam,
            ':prijs' => $prijs
        ));

        $laatsteId = $dbh->lastInsertId();

        if ($laatsteId > 0) {
            /* Update worked because query affected X amount of rows. */
            $result = $laatsteId;
        } else {
            $error = $dbh->errorInfo(); // Toont de DB-error
            $result = false;
            // TODO: exception omwille van fout bij creatie van bestelling 
        }

        // Verbreek connectie
        $dbh = null;

        // Return msg
        return $result;
    }

    public function update($idBodem, $idSamenstelling, $naam, $prijs)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`bodem` 
            SET `idSamenstelling` = :idSamenstelling, `naam` = :naam, `prijs` = :':prijs'
            WHERE (`idBodem` = :idBodem)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBodem' => $idBodem,
            ':idSamenstelling' => $idSamenstelling,
            ':naam' => $naam,
            ':prijs' => $prijs
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }
}
