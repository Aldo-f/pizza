<?php
namespace pizza\data\product;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\product\ProductLijn;

class ProductLijnDAO
{

    // getAll
    // getById
    // create
    // update

    public function getAll()
    {
        $sql =
            "SELECT idProductLijn, idBedrijf, idProduct, beschikbaar 
            FROM aldofi1q_opleiding_pizza.productlijn";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = ProductLijn::create(
                $rij["idProductLijn"],
                $rij["idBedrijf"],
                $rij["idProduct"],
                $rij["beschikbaar"]
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getAllAvailable()
    {
        $sql =
            "SELECT idProductLijn, idBedrijf, idProduct, beschikbaar 
            FROM aldofi1q_opleiding_pizza.productlijn
            WHERE beschikbaar > 0";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = ProductLijn::create(
                $rij["idProductLijn"],
                $rij["idBedrijf"],
                $rij["idProduct"],
                $rij["beschikbaar"]
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getById($id)
    {
        $sql =
            "SELECT idProductLijn, idBedrijf, idProduct, beschikbaar 
            FROM aldofi1q_opleiding_pizza.productlijn
            WHERE idProductLijn = :id";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = ProductLijn::create(
                    $rij["idProductLijn"],
                    $rij["idBedrijf"],
                    $rij["idProduct"],
                    $rij["beschikbaar"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function getByIdBedrijf($id)
    {
        $sql =
            "SELECT idProductLijn, idBedrijf, idProduct, beschikbaar 
            FROM aldofi1q_opleiding_pizza.productlijn
            WHERE idBedrijf = :id";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = ProductLijn::create(
                    $rij["idProductLijn"],
                    $rij["idBedrijf"],
                    $rij["idProduct"],
                    $rij["beschikbaar"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function getByIdBedrijfAvailable($id)
    {
        $sql =
            "SELECT idProductLijn, idBedrijf, idProduct, beschikbaar 
            FROM aldofi1q_opleiding_pizza.productlijn
            WHERE idBedrijf = :id AND beschikbaar > 0";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = ProductLijn::create(
                    $rij["idProductLijn"],
                    $rij["idBedrijf"],
                    $rij["idProduct"],
                    $rij["beschikbaar"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function getByIdProduct($id)
    {
        $sql =
            "SELECT idProductLijn, idBedrijf, idProduct, beschikbaar 
            FROM aldofi1q_opleiding_pizza.productlijn
            WHERE idProduct = :id";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = ProductLijn::create(
                    $rij["idProductLijn"],
                    $rij["idBedrijf"],
                    $rij["idProduct"],
                    $rij["beschikbaar"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }
    public function getByIdProductAndIdBedrijf($idProduct, $idBedrijf)
    {
        $sql =
            "SELECT idProductLijn, idBedrijf, idProduct, beschikbaar 
            FROM aldofi1q_opleiding_pizza.productlijn
            WHERE idProduct = :idProduct AND idBedrijf = :idBedrijf";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idProduct' => $idProduct,
            ':idBedrijf' => $idBedrijf
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = ProductLijn::create(
                    $rij["idProductLijn"],
                    $rij["idBedrijf"],
                    $rij["idProduct"],
                    $rij["beschikbaar"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function create($idBedrijf, $idProduct, $beschikbaar)
    {

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "INSERT INTO `aldofi1q_opleiding_pizza`.`productlijn` (`idBedrijf`, `idProduct`, `beschikbaar`) 
            VALUES (:idBedrijf, :idProduct, :beschikbaar)";

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBedrijf' => $idBedrijf,
            ':idProduct' => $idProduct,
            ':beschikbaar' => $beschikbaar
        ));

        $laatsteId = $dbh->lastInsertId();

        if ($laatsteId > 0) {
            /* Update worked because query affected X amount of rows. */
            $result = $laatsteId;
        } else {
            $error = $dbh->errorInfo(); // Toont de DB-error
            $result = false;
            // TODO: exception omwille van fout bij creatie van bestelling 
        }

        // Verbreek connectie
        $dbh = null;

        // Return msg
        return $result;
    }

    public function update($idProductLijn, $idBedrijf, $idProduct, $beschikbaar)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`productlijn` 
            SET `idProduct` = :idProduct, `beschikbaar` = :beschikbaar, `idBedrijf` = :idBedrijf
            WHERE (`idProductLijn` = :idProductLijn)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idProductLijn' => $idProductLijn,
            ':idBedrijf' => $idBedrijf,
            ':idProduct' => $idProduct,
            ':beschikbaar' => $beschikbaar
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }

    public function updateByBedrijfId($idBedrijf, $idProduct, $beschikbaar)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`productlijn` 
            SET `idProduct` = :idProduct, `beschikbaar` = :beschikbaar
            WHERE (`idBedrijf` = :idBedrijf)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBedrijf' => $idBedrijf,
            ':idProduct' => $idProduct,
            ':beschikbaar' => $beschikbaar
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }

    public function updateByProductId($idBedrijf, $idProduct, $beschikbaar)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`productlijn` 
            SET `idBedrijf` = :idBedrijf, `beschikbaar` = :beschikbaar
            WHERE (`idProduct` = :idProduct)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBedrijf' => $idBedrijf,
            ':idProduct' => $idProduct,
            ':beschikbaar' => $beschikbaar
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }
}
