<?php
namespace pizza\data\product;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\product\Product;
use pizza\business\product\SausService;
use pizza\business\product\BodemService;
use pizza\business\product\FormaatService;
use pizza\business\product\ProductService;
use pizza\business\product\IngredientService;
use pizza\business\product\IngredientLijnService;

class ProductDAO
{

    // getAll
    // getById
    // create
    // update

    public function getAll()
    {
        $sql =
            "SELECT idProduct, naam, omschrijving, idFormaat, idBodem, idSaus 
            FROM aldofi1q_opleiding_pizza.product";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = Product::create(
                $rij["idProduct"],
                $rij["naam"],
                $rij["omschrijving"],
                $rij["idFormaat"],
                $rij["idBodem"],
                $rij["idSaus"]
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getById($id)
    {
        $sql =
            "SELECT idProduct, naam, omschrijving, idFormaat, idBodem, idSaus 
            FROM aldofi1q_opleiding_pizza.product
            WHERE idProduct = :id";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$rij) {
            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $item = Product::create(
                $rij["idProduct"],
                $rij["naam"],
                $rij["omschrijving"],
                $rij["idFormaat"],
                $rij["idBodem"],
                $rij["idSaus"]
            );
            return $item;
        }
    }

    /**
     * Deze functie toont een volledig product 
     * op basis van een idProduct
     *
     * @param  $id 
     * @return $volledigProduct
     */
    public function getProductById($id)
    {
        $ingredientLijnSvc = new IngredientLijnService;
        $productSvc = new ProductService;
        $ingredientSvc = new IngredientService;
        $formaatSvc = new FormaatService;
        $bodemSvc = new BodemService;
        $sausSvc = new SausService;

        $mijnProduct = $productSvc->getById($id);

        $mijnIdFormaat = $mijnProduct->getIdFormaat();
        $mijnIdBodem = $mijnProduct->getIdBodem();
        $mijnIdSaus = $mijnProduct->getIdSaus();

        $mijnIngredientLijnLijst = $ingredientLijnSvc->getByIdProduct($id);
        $mijnIngredienten = [];
        $mijnIngredientPrijs = 0;
        foreach ($mijnIngredientLijnLijst as $ingredientLijn) {
            $mijnIdIngredient = $ingredientLijn->getIdIngredient();
            $mijnIngredient = $ingredientSvc->getById($mijnIdIngredient);
            $mijnIngredientPrijs += +$mijnIngredient->getPrijs();

            array_push($mijnIngredienten, $mijnIngredient);
        }

        $mijnFormaat = $formaatSvc->getById($mijnIdFormaat);
        $mijnBodem = $bodemSvc->getById($mijnIdBodem);
        $mijnSaus = $sausSvc->getById($mijnIdSaus);

        $mijnFormaatPrijs = $mijnFormaat->getPrijs();
        $mijnBodemPrijs = $mijnBodem->getPrijs();
        $mijnSausPrijs = $mijnSaus->getPrijs();

        $totaal = $mijnBodemPrijs + $mijnFormaatPrijs + $mijnSausPrijs + $mijnIngredientPrijs;

        $items = (object)[
            $mijnFormaat, $mijnBodem, $mijnSaus, $mijnIngredienten
        ];

        $volledigProduct = (object)[
            "product" => $mijnProduct,
            "bestandsdelen" => $items,
            "prijs" => $totaal
        ];

        return $volledigProduct;
    }

    public function create($naam, $omschrijving, $idFormaat, $idBodem, $idSaus)
    {

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "INSERT INTO `aldofi1q_opleiding_pizza`.`product` (`naam`, `omschrijving`, `idFormaat`, `idBodem`, `idSaus`) 
            VALUES (:naam, :omschrijving, :idFormaat, :idBodem, :idSaus)";


        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':naam' => $naam,
            ':omschrijving' => $omschrijving,
            ':idFormaat' => $idFormaat,
            ':idBodem' => $idBodem,
            ':idSaus' => $idSaus
        ));

        $laatsteId = $dbh->lastInsertId();

        if ($laatsteId > 0) {
            /* Update worked because query affected X amount of rows. */
            $result = $laatsteId;
        } else {
            $error = $dbh->errorInfo(); // Toont de DB-error
            $result = false;
            // TODO: exception omwille van fout bij creatie van Product 
        }

        // Verbreek connectie
        $dbh = null;

        // Return msg
        return $result;
    }

    public function update($idProduct, $naam, $omschrijving, $idFormaat, $idBodem, $idSaus)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`product` 
            SET `naam` = :naam, `omschrijving` = :omschrijving, `idFormaat` = :':idFormaat', `idBodem` = :idBodem, `idSaus` = :idSaus
            WHERE (`idProduct` = :idProduct)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idProduct' => $idProduct,
            ':naam' => $naam,
            ':omschrijving' => $omschrijving,
            ':idFormaat' => $idFormaat,
            ':idBodem' => $idBodem,
            ':idSaus' => $idSaus
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }
}
