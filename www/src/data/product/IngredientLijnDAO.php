<?php
namespace pizza\data\product;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\product\IngredientLijn;

// OPGELET: in DB: IngridientLijn (met "i" i.p.v. "e")...


class IngredientLijnDAO
{

    // getAll
    // getById
    // create
    // update

    public function getAll()
    {
        $sql =
            "SELECT idIngredient, idProduct 
            FROM aldofi1q_opleiding_pizza.ingridientlijn";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = IngredientLijn::create(
                $rij["idIngredient"],
                $rij["idProduct"]
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getByIdProduct($id)
    {
        $sql =
            "SELECT idIngredient, idProduct 
            FROM aldofi1q_opleiding_pizza.ingridientlijn
            WHERE idProduct = :id";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = IngredientLijn::create(
                    $rij["idIngredient"],
                    $rij["idProduct"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function getByIdIngredient($id)
    {
        $sql =
            "SELECT idIngredient, idProduct
            FROM aldofi1q_opleiding_pizza.ingridientlijn
            WHERE idIngredient = :id;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$rij) {
            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $item = IngredientLijn::create(
                $rij["idIngredient"],
                $rij["idProduct"]
            );
            return $item;
        }
    }

    public function create($idIngredient, $idProduct)
    {

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "INSERT INTO `aldofi1q_opleiding_pizza`.`ingridientlijn` (`idIngredient`, `idProduct`) 
            VALUES (:idIngredient, :idProduct)";

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idIngredient' => $idIngredient,
            ':idProduct' => $idProduct
        ));

        $laatsteId = $dbh->lastInsertId();

        if ($laatsteId > 0) {
            /* Update worked because query affected X amount of rows. */
            $result = $laatsteId;
        } else {
            $error = $dbh->errorInfo(); // Toont de DB-error
            $result = false;
            // TODO: exception omwille van fout bij creatie van bestelling 
        }

        // Verbreek connectie
        $dbh = null;

        // Return msg
        return $result;
    }

    public function updateByProductId($idProduct, $idIngredient)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`ingridientlijn` 
            SET `idIngredient` = :idIngredient
            WHERE (`idProduct` = :idProduct)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idIngredient' => $idIngredient,
            ':idProduct' => $idProduct
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }

    public function updateByIngredientId($idIngredient, $idProduct)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`ingridientlijn` 
            SET `idProduct` = :idProduct
            WHERE (`idIngredient` = :idIngredient)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idIngredient' => $idIngredient,
            ':idProduct' => $idProduct
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }
}
