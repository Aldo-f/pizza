<?php
namespace pizza\data\product;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\product\Formaat;

class FormaatDAO
{
    // getAll
    // getById
    // create
    // update

    public function getAll()
    {
        $sql =
            "SELECT idFormaat, idSamenstelling, naam, prijs 
            FROM aldofi1q_opleiding_pizza.formaat";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = Formaat::create(
                $rij["idFormaat"],
                $rij["idSamenstelling"],
                $rij["naam"],
                $rij["prijs"]
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getById($id)
    {
        $sql =
            "SELECT idFormaat, idSamenstelling, naam, prijs 
            FROM aldofi1q_opleiding_pizza.formaat
            WHERE idFormaat = :id;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$rij) {
            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $item = Formaat::create(
                $rij["idFormaat"],
                $rij["idSamenstelling"],
                $rij["naam"],
                $rij["prijs"]
            );
            return $item;
        }
    }

    public function create($idSamenstelling, $naam, $prijs)
    {

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "INSERT INTO `aldofi1q_opleiding_pizza`.`formaat` (`idSamenstelling`, `naam`, `prijs`) 
            VALUES (:idSamenstelling, :naam, :prijs)";


        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idSamenstelling' => $idSamenstelling,
            ':naam' => $naam,
            ':prijs' => $prijs
        ));

        $laatsteId = $dbh->lastInsertId();

        if ($laatsteId > 0) {
            /* Update worked because query affected X amount of rows. */
            $result = $laatsteId;
        } else {
            $error = $dbh->errorInfo(); // Toont de DB-error
            $result = false;
            // TODO: exception omwille van fout bij creatie van bestelling 
        }

        // Verbreek connectie
        $dbh = null;

        // Return msg
        return $result;
    }

    public function update($idFormaat, $idSamenstelling, $naam, $prijs)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`formaat` 
            SET `idSamenstelling` = :idSamenstelling, `naam` = :naam, `prijs` = :':prijs'
            WHERE (`idFormaat` = :idFormaat)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idFormaat' => $idFormaat,
            ':idSamenstelling' => $idSamenstelling,
            ':naam' => $naam,
            ':prijs' => $prijs
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }
}
