<?php
namespace pizza\data;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\Account;

class AccountDAO
{
    private function firstLetterUp($text)
    {
        return ucwords(strtolower($text));
    }

    private function maakHached($data)
    {
        // pas de data aan met gegevens enkel uit de data
        $toHash  = strlen($data) ** (strlen($data) + 1);
        $toHash .= $data;

        // hash de data
        $hashedValue = hash('sha512', $toHash, false);
        return $hashedValue;
    }

    public function getAll()
    {
        $sql =
            "SELECT idAccount, naam, voornaam, adres, huisnummer, telefoonnummer, email, wachtwoord, geboortedatum, idPlaats
            FROM aldofi1q_opleiding_pizza.account;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = Account::create(
                $rij["idAccount"],
                $rij["naam"],
                $rij['voornaam'],
                $rij['adres'],
                $rij['huisnummer'],
                $rij['telefoonnummer'],
                $rij['email'],
                $rij['wachtwoord'],
                $rij['geboortedatum'],
                $rij['idPlaats']
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getById($id)
    {
        $sql =
            "SELECT idAccount, naam, voornaam, adres, huisnummer, telefoonnummer, email, wachtwoord, geboortedatum, idPlaats
            FROM aldofi1q_opleiding_pizza.account
            WHERE idAccount = :id";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$rij) {
            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $item = Account::create(
                $rij["idAccount"],
                $rij["naam"],
                $rij['voornaam'],
                $rij['adres'],
                $rij['huisnummer'],
                $rij['telefoonnummer'],
                $rij['email'],
                $rij['wachtwoord'],
                $rij['geboortedatum'],
                $rij['idPlaats']
            );
            $dbh = null;
            return $item;
        }
    }

    public function getByEmail($email)
    {
        $email = strtolower($email);

        $sql =
            "SELECT idAccount
            FROM aldofi1q_opleiding_pizza.account
            WHERE email = :email;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':email' => $email
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$rij) {
            // Er bestaat geen account met dit emailadres
            return null;
        } else {
            // Er bestaat een account           

            // TODO: Exception omdat er reeds een account is met dit emailadres
            return $rij["idAccount"];
        }
    }

    public function getByEmailWachtwoord($email, $wachtwoord)
    {
        $email = strtolower($email);
        $wachtwoord = self::maakHached($wachtwoord);

        $sql =
            "SELECT idAccount
            FROM aldofi1q_opleiding_pizza.account
            WHERE email = :email AND wachtwoord = :wachtwoord";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':email' => $email,
            ':wachtwoord' => $wachtwoord
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        $dbh = null;

        if ($rij) {
            // $idAccount werd gevonden, haal nu $account op
            $account = self::getById($rij["idAccount"]);
            return $account;
        } else {
            // Er bestaat geen account        

            // TODO: Exception omdat login incorrect is

            return null;
        }
    }

    public function create($naam, $voornaam, $adres, $huisnummer, $telefoonnummer, $email, $wachtwoord, $geboortedatum, $idPlaats)
    {
        // Doe hier controle van email
        $idAccount = self::getByEmail($email);

        if (!$idAccount) {
            // pas inputs aan
            $naam = self::firstLetterUp($naam);
            $voornaam = self::firstLetterUp($voornaam);
            $adres = self::firstLetterUp($adres);
            $email = strtolower($email);
            $wachtwoord = self::maakHached($wachtwoord);

            $dbh = new PDO(
                DBconfig::$DB_CONNSTRING,
                DBconfig::$DB_USERNAME,
                DBconfig::$DB_PASSWORD
            );

            $sql =
                "INSERT INTO `aldofi1q_opleiding_pizza`.`account` (`naam`, `voornaam`, `adres`, `huisnummer`, `telefoonnummer`, `email`, `wachtwoord`, `geboortedatum`, `idPlaats`) 
            VALUES (:naam, :voornaam , :adres, :huisnummer, :telefoonnummer, :email, :wachtwoord, :geboortedatum, :idPlaats )";


            $stmt = $dbh->prepare($sql);
            $stmt->execute(array(
                ':naam' => $naam,
                ':voornaam' => $voornaam,
                ':adres' => $adres,
                ':huisnummer' => $huisnummer,
                ':telefoonnummer' => $telefoonnummer,
                ':email' => $email,
                ':wachtwoord' => $wachtwoord,
                ':geboortedatum' => $geboortedatum,
                ':idPlaats' => $idPlaats
            ));

            $laatsteId = $dbh->lastInsertId();

            if ($laatsteId > 0) {
                /* Update worked because query affected X amount of rows. */
                $result = $laatsteId;
            } else {
                $error = $dbh->errorInfo(); // Toont de DB-error
                $result = false;
                // TODO: exception omwille van fout bij creatie account 
            }

            // Verbreek connectie
            $dbh = null;

            // Return msg
            return $result;
        }
    }

    public function update($idAccount, $naam, $voornaam, $adres, $huisnummer, $telefoonnummer, $email, $wachtwoord, $geboortedatum, $idPlaats)
    {
        $email = strtolower($email);
        $wachtwoord = self::maakHached($wachtwoord);

        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`account` 
            SET `naam` = :naam, `voornaam` = :voornaam, `adres` = :adres, `huisnummer` = :huisnummer, `telefoonnummer` = :telefoonnummer , `email` = :email , `wachtwoord` = :wachtwoord , `geboortedatum` = :geboortedatum , `idPlaats` = :idPlaats 
            WHERE (`idAccount` = :idAccount)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            'idAccount' => $idAccount,
            ':naam' => $naam,
            ':voornaam' => $voornaam,
            ':adres' => $adres,
            ':huisnummer' => $huisnummer,
            ':telefoonnummer' => $telefoonnummer,
            ':email' => $email,
            ':wachtwoord' => $wachtwoord,
            ':geboortedatum' => $geboortedatum,
            ':idPlaats' => $idPlaats
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        // $laatsteId = $dbh->lastInsertId();
        // $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($stmt) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $idAccount;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }
}
