<?php
namespace pizza\data\bedrijf;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\bedrijf\LeverLijn;

class LeverLijnDAO
{

    // getAll
    // getById
    // create
    // update

    public function getAll()
    {
        $sql =
            "SELECT idBedrijf, idPlaats 
            FROM aldofi1q_opleiding_pizza.leverlijn";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = LeverLijn::create(
                $rij["idBedrijf"],
                $rij["idPlaats"]
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getByIdBedrijf($id)
    {
        $sql =
            "SELECT idBedrijf, idPlaats 
            FROM aldofi1q_opleiding_pizza.leverlijn
            WHERE idBedrijf = :id";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = LeverLijn::create(
                    $rij["idBedrijf"],
                    $rij["idPlaats"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function getByIdPlaats($id)
    {
        $sql =
            "SELECT idBedrijf, idPlaats 
            FROM aldofi1q_opleiding_pizza.leverlijn
            WHERE idPlaats = :id";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = LeverLijn::create(
                    $rij["idBedrijf"],
                    $rij["idPlaats"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }


    public function getByPostcode($postcode)
    {
        $sql =
            "SELECT idBedrijf, leverlijn.idPlaats, postcode, plaats 
            FROM aldofi1q_opleiding_pizza.leverlijn
            INNER JOIN aldofi1q_opleiding_pizza.plaats on plaats.idPlaats = leverlijn.idPlaats
            WHERE postcode = :postcode";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':postcode' => $postcode
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = LeverLijn::create(
                    $rij["idBedrijf"],
                    $rij["idPlaats"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function create($idBedrijf, $idPlaats)
    {

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "INSERT INTO `aldofi1q_opleiding_pizza`.`leverlijn` (`idBedrijf`, `idPlaats`) 
            VALUES (:idBedrijf, :idPlaats)";

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBedrijf' => $idBedrijf,
            ':idPlaats' => $idPlaats
        ));

        $laatsteId = $dbh->lastInsertId();

        if ($laatsteId > 0) {
            /* Update worked because query affected X amount of rows. */
            $result = $laatsteId;
        } else {
            $error = $dbh->errorInfo(); // Toont de DB-error
            $result = false;
            // TODO: exception omwille van fout bij creatie van bestelling 
        }

        // Verbreek connectie
        $dbh = null;

        // Return msg
        return $result;
    }

    public function updateByBedrijfId($idBedrijf, $idPlaats)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`leverlijn` 
            SET `idPlaats` = :idPlaats 
            WHERE (`idBedrijf` = :idBedrijf)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBedrijf' => $idBedrijf,
            ':idPlaats' => $idPlaats
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            // krijgt dit een id?... 
            $result = $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }

    public function updateByPlaatsId($idBedrijf, $idPlaats)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`leverlijn` 
            SET `idBedrijf` = :idBedrijf 
            WHERE (`idPlaats` = :idPlaats)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBedrijf' => $idBedrijf,
            ':idPlaats' => $idPlaats
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }
}