<?php
namespace pizza\data\bedrijf;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\bedrijf\Bedrijf;

class BedrijfDAO
{

    // getAll
    // getById
    // create
    // update

    public function getAll()
    {
        $sql =
            "SELECT idBedrijf, bedrijfnaam, adres, huisnummer, telefoonnummer, idPlaats 
            FROM aldofi1q_opleiding_pizza.bedrijf";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = Bedrijf::create(
                $rij["idBedrijf"],
                $rij["bedrijfnaam"],
                $rij["adres"],
                $rij["huisnummer"],
                $rij["telefoonnummer"],
                $rij["idPlaats"]
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getById($id)
    {
        $sql =
            "SELECT idBedrijf, bedrijfnaam, adres, huisnummer, telefoonnummer, idPlaats 
            FROM aldofi1q_opleiding_pizza.bedrijf
            WHERE idBedrijf = :id;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$rij) {
            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $item = Bedrijf::create(
                $rij["idBedrijf"],
                $rij["bedrijfnaam"],
                $rij["adres"],
                $rij["huisnummer"],
                $rij["telefoonnummer"],
                $rij["idPlaats"]
            );
            return $item;
        }
    }

    public function create($bedrijfnaam, $adres, $huisnummer, $telefoonnummer, $idPlaats)
    {

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "INSERT INTO `aldofi1q_opleiding_pizza`.`bedrijf` (`bedrijfnaam`, `adres`, `huisnummer`, `telefoonnummer`, `idPlaats`) 
            VALUES (:bedrijfnaam, :adres, :huisnummer, :telefoonnummer, :idPlaats)";

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':bedrijfnaam' => $bedrijfnaam,
            ':adres' => $adres,
            ':huisnummer' => $huisnummer,
            ':telefoonnummer' => $telefoonnummer,
            ':idPlaats' => $idPlaats,
        ));

        $laatsteId = $dbh->lastInsertId();

        if ($laatsteId > 0) {
            /* Update worked because query affected X amount of rows. */
            $result = $laatsteId;
        } else {
            $error = $dbh->errorInfo(); // Toont de DB-error
            $result = false;
            // TODO: exception omwille van fout bij creatie van bestelling 
        }

        // Verbreek connectie
        $dbh = null;

        // Return msg
        return $result;
    }

    public function update($idBedrijf, $bedrijfnaam, $adres, $huisnummer, $telefoonnummer, $idPlaats)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`bedrijf` 
            SET `bedrijfnaam` = :bedrijfnaam, `adres` = :adres, `huisnummer` = :huisnummer, `telefoonnummer` = :telefoonnummer, `idPlaats` = :idPlaats
            WHERE (`idBedrijf` = :idBedrijf)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBedrijf' => $idBedrijf,
            ':bedrijfnaam' => $bedrijfnaam,
            ':adres' => $adres,
            ':huisnummer' => $huisnummer,
            ':telefoonnummer' => $telefoonnummer,
            ':idPlaats' => $idPlaats,
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }
}
