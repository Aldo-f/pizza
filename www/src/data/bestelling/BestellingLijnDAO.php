<?php
namespace pizza\data\bestelling;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\bestelling\BestellingLijn;


class BestellingLijnDAO
{

    // getAll
    // getById
    // create
    // update

    public function getAll()
    {
        $sql =
            "SELECT idPromotieLijn, idProductLijn, idBestelling, aantalProductLijn, aantalPromotieLijn 
            FROM aldofi1q_opleiding_pizza.bestellinglijn";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = BestellingLijn::create(
                $rij["idPromotieLijn"],
                $rij["idProductLijn"],
                $rij["idBestelling"],
                $rij["aantalProductLijn"],
                $rij["aantalPromotieLijn"]
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getByIdPromotieLijn($id)
    {
        $sql =
            "SELECT idPromotieLijn, idProductLijn, idBestelling, aantalProductLijn, aantalPromotieLijn 
            FROM aldofi1q_opleiding_pizza.bestellinglijn
            WHERE idPromotieLijn = :id";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = BestellingLijn::create(
                    $rij["idPromotieLijn"],
                    $rij["idProductLijn"],
                    $rij["idBestelling"],
                    $rij["aantalProductLijn"],
                    $rij["aantalPromotieLijn"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function getByIdProductLijn($id)
    {
        $sql =
            "SELECT idPromotieLijn, idProductLijn, idBestelling, aantalProductLijn, aantalPromotieLijn 
            FROM aldofi1q_opleiding_pizza.bestellinglijn
            WHERE idProductLijn = :id";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = BestellingLijn::create(
                    $rij["idPromotieLijn"],
                    $rij["idProductLijn"],
                    $rij["idBestelling"],
                    $rij["aantalProductLijn"],
                    $rij["aantalPromotieLijn"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function getByIdBestelling($id)
    {
        $sql =
            "SELECT idPromotieLijn, idProductLijn, idBestelling, aantalProductLijn, aantalPromotieLijn 
            FROM aldofi1q_opleiding_pizza.bestellinglijn
            WHERE idBestelling = :id";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));

        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = BestellingLijn::create(
                    $rij["idPromotieLijn"],
                    $rij["idProductLijn"],
                    $rij["idBestelling"],
                    $rij["aantalProductLijn"],
                    $rij["aantalPromotieLijn"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function create($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn)
    {

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "INSERT INTO `aldofi1q_opleiding_pizza`.`bestellinglijn` (`idPromotieLijn`, `idProductLijn`, `idBestelling`, `aantalProductLijn`, `aantalPromotieLijn`) 
            VALUES (:idPromotieLijn, :idProductLijn, :idBestelling, :aantalProductLijn, :aantalPromotieLijn)";

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idPromotieLijn' => $idPromotieLijn,
            ':idProductLijn' => $idProductLijn,
            ':idBestelling' => $idBestelling,
            ':aantalProductLijn' => $aantalProductLijn,
            ':aantalPromotieLijn' => $aantalPromotieLijn
        ));

        $laatsteId = $dbh->lastInsertId();

        if ($laatsteId > 0) {
            /* Update worked because query affected X amount of rows. */
            $result = $laatsteId;
        } else {
            $error = $dbh->errorInfo(); // Toont de DB-error
            $result = false;
            // TODO: exception omwille van fout bij creatie van bestelling 
        }

        // Verbreek connectie
        $dbh = null;

        // Return msg
        return $result;
    }

    public function createBasic($idProductLijn, $idBestelling, $aantalProductLijn)
    {

        $aantalProductLijn = (string)$aantalProductLijn;

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "INSERT INTO `aldofi1q_opleiding_pizza`.`bestellinglijn` (`idProductLijn`, `idBestelling`, `aantalProductLijn`) 
            VALUES (:idProductLijn, :idBestelling, :aantalProductLijn)";

        // "INSERT INTO `aldofi1q_opleiding_pizza`.`bestellinglijn` (`idPromotieLijn`, `idProductLijn`, `idBestelling`, `aantalProductLijn`, `aantalPromotieLijn`) 
        // VALUES (:idProductLijn, :idBestelling, :aantalProductLijn)";

        // "INSERT INTO `aldofi1q_opleiding_pizza`.`bestellinglijn` (`idProductLijn`, `idBestelling`, `aantalProductLijn`) 
        // VALUES (:idBedrijf, :idProduct, :aantalProductLijn)";

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idProductLijn' => $idProductLijn,
            ':idBestelling' => $idBestelling,
            ':aantalProductLijn' => $aantalProductLijn,
        ));

        $laatsteId = $dbh->lastInsertId();

        if ($laatsteId > 0) {
            /* Update worked because query affected X amount of rows. */
            $result = $laatsteId;
        } else {
            $error = $dbh->errorInfo(); // Toont de DB-error
            $result = false;
            // TODO: exception omwille van fout bij creatie van bestelling 
        }

        // Verbreek connectie
        $dbh = null;

        // Return msg
        return $result;
    }

    // Nog eventueel verder aan te vullen
    public function updateByIdPromotieLijn($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`productlijn` 
            SET `idProduct` = :idProduct, `beschikbaar` = :beschikbaar, `idBedrijf` = :idBedrijf
            WHERE (`idProductLijn` = :idProductLijn)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idPromotieLijn' => $idPromotieLijn,
            ':idProductLijn' => $idProductLijn,
            ':idBestelling' => $idBestelling,
            ':aantalProductLijn' => $aantalProductLijn,
            ':aantalPromotieLijn' => $aantalPromotieLijn
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }

    public function updateByBedrijfId($idBedrijf, $idProduct, $beschikbaar)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`productlijn` 
            SET `idProduct` = :idProduct, `beschikbaar` = :beschikbaar
            WHERE (`idBedrijf` = :idBedrijf)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBedrijf' => $idBedrijf,
            ':idProduct' => $idProduct,
            ':beschikbaar' => $beschikbaar
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }

    public function updateByProductId($idBedrijf, $idProduct, $beschikbaar)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`productlijn` 
            SET `idBedrijf` = :idBedrijf, `beschikbaar` = :beschikbaar
            WHERE (`idProduct` = :idProduct)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBedrijf' => $idBedrijf,
            ':idProduct' => $idProduct,
            ':beschikbaar' => $beschikbaar
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }
}
