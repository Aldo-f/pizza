<?php
namespace pizza\data\bestelling;

use PDO;
use pizza\data\DBconfig;
use pizza\entities\product\ProductLijn;
use pizza\entities\bestelling\Bestelling;
use pizza\entities\bestelling\BestellingLijn;

class BestellingDAO
{

    // getAll
    // getById
    // create
    // update

    public function getAll()
    {
        $sql =
            "SELECT idBestelling, idAccount, bestelTijd, leveringsTijd, mededeling, prijs
            FROM aldofi1q_opleiding_pizza.bestelling";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = Bestelling::create(
                $rij["idBestelling"],
                $rij["idAccount"],
                $rij["bestelTijd"],
                $rij["leveringsTijd"],
                $rij["mededeling"],
                $rij["prijs"]
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getBasicAll()
    {
        $sql =
            "SELECT bestelling.idBestelling, idAccount, bestelTijd, leveringsTijd, mededeling, prijs, idPromotieLijn, productlijn.idProductLijn, aantalProductLijn, aantalPromotieLijn, idBedrijf, idProduct, beschikbaar 
            FROM aldofi1q_opleiding_pizza.bestelling
            inner join aldofi1q_opleiding_pizza.bestellinglijn on bestelling.idBestelling = bestellinglijn.idbestelling
            inner join aldofi1q_opleiding_pizza.productlijn on bestellinglijn.idproductlijn = productlijn.idproductlijn
            ORDER BY bestelling.idBestelling";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $resultSet = $dbh->query($sql);

        $lijst = array();
        // $i = 0;
        foreach ($resultSet as $rij) {

            // if ($i == 0) {
            $bestelling = Bestelling::create(
                $rij["idBestelling"],
                $rij["idAccount"],
                $rij["bestelTijd"],
                $rij["leveringsTijd"],
                $rij["mededeling"],
                $rij["prijs"]
            );
            // }

            $bestelingLijn = BestellingLijn::create(
                $rij["idPromotieLijn"],
                $rij["idProductLijn"],
                $rij["idBestelling"],
                $rij["aantalProductLijn"],
                $rij["aantalPromotieLijn"]
            );
            $productLijn = ProductLijn::create(
                $rij["idProductLijn"],
                $rij["idBedrijf"],
                $rij["idProduct"],
                $rij["beschikbaar"]
            );

            // if ($i == 0) {

            $item = (object)[
                "bestelling" => $bestelling,
                "bestelingLijn" => $bestelingLijn,
                "productLijn" => $productLijn
            ];
            // $i++;
            // } else {

            //     $item = (object)[
            //         "bestelingLijn" => $bestelingLijn,
            //         "productLijn" => $productLijn
            //     ];
            // }


            array_push($lijst, $item);
        }
        $dbh = null;
        return $lijst;
    }

    public function getById($id)
    {
        $sql =
            "SELECT idBestelling, idAccount, bestelTijd, leveringsTijd, mededeling, idBedrijf
            FROM aldofi1q_opleiding_pizza.bestelling
            WHERE idBestelling = :id;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$rij) {
            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $item = Bestelling::create(
                $rij["idBestelling"],
                $rij["idAccount"],
                $rij["bestelTijd"],
                $rij["leveringsTijd"],
                $rij["mededeling"],
                $rij["idBedrijf"]
            );
            return $item;
        }
    }

    public function getBasicById($id)
    {
        $sql =
            "SELECT bestelling.idBestelling, idAccount, bestelTijd, leveringsTijd, mededeling, prijs, idPromotieLijn, productlijn.idProductLijn, aantalProductLijn, aantalPromotieLijn, idBedrijf, idProduct, beschikbaar 
            FROM aldofi1q_opleiding_pizza.bestelling
            inner join aldofi1q_opleiding_pizza.bestellinglijn on bestelling.idBestelling = bestellinglijn.idbestelling
            inner join aldofi1q_opleiding_pizza.productlijn on bestellinglijn.idproductlijn = productlijn.idproductlijn
            WHERE bestelling.idBestelling = :id
            ORDER BY bestelling.idBestelling";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$rij) {
            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $item = Bestelling::create(
                $rij["idBestelling"],
                $rij["idAccount"],
                $rij["bestelTijd"],
                $rij["leveringsTijd"],
                $rij["mededeling"],
                $rij["prijs"]
            );
            return $item;
        }
    }

    public function getByIdAccount($id)
    {
        $sql =
            "SELECT idBestelling, idAccount, bestelTijd, leveringsTijd, mededeling, prijs
            FROM aldofi1q_opleiding_pizza.bestelling
            WHERE idAccount = :id;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();
            foreach ($resultSet as $rij) {
                $item = Bestelling::create(
                    $rij["idBestelling"],
                    $rij["idAccount"],
                    $rij["bestelTijd"],
                    $rij["leveringsTijd"],
                    $rij["mededeling"],
                    $rij["prijs"]
                );
                array_push($lijst, $item);
            }
            return $lijst;
        }
    }

    public function  getBasicByIdAccount($id)
    {
        $sql =
            "SELECT bestelling.idBestelling, idAccount, bestelTijd, leveringsTijd, mededeling, prijs, idPromotieLijn, productlijn.idProductLijn, aantalProductLijn, aantalPromotieLijn, idBedrijf, idProduct, beschikbaar 
            FROM aldofi1q_opleiding_pizza.bestelling
            inner join aldofi1q_opleiding_pizza.bestellinglijn on bestelling.idBestelling = bestellinglijn.idbestelling
            inner join aldofi1q_opleiding_pizza.productlijn on bestellinglijn.idproductlijn = productlijn.idproductlijn
            WHERE bestelling.idAccount  = :idAccount
            ORDER BY bestelling.bestelTijd desc ";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idAccount' => $id
        ));
        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // controleer hoeveel rows er zijn:
        //  $resultSet = $dbh->query($sql);
        //  $aantalRows = $resultSet->rowCount();

        $dbh = null;

        if (!$resultSet) {

            return null;
            // TODO: exception omdat id niet bestaat
        } else {
            $lijst = array();

            $bestellingLijnLijst = [];
            $vorigeIdBestelling = -1;

            foreach ($resultSet as $rij) {

                $huidigIdBestelling = $rij["idBestelling"];

                // of ook wanneer het de laatse is
                if ($huidigIdBestelling != $vorigeIdBestelling) {
                    $bestelling = Bestelling::create(
                        $rij["idBestelling"],
                        $rij["idAccount"],
                        $rij["bestelTijd"],
                        $rij["leveringsTijd"],
                        $rij["mededeling"],
                        $rij["prijs"]
                    );
                }
                $vorigeIdBestelling = $huidigIdBestelling;

                $bestellinglijn = BestellingLijn::create(
                    $rij["idPromotieLijn"],
                    $rij["idProductLijn"],
                    $rij["idBestelling"],
                    $rij["aantalProductLijn"],
                    $rij["aantalPromotieLijn"]
                );
                array_push($bestellingLijnLijst, $bestellinglijn);
            }
            return $bestellingLijnLijst;
        }
    }

    public function create($idAccount, $leveringsTijd, $mededeling, $prijs)
    {

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "INSERT INTO `aldofi1q_opleiding_pizza`.`bestelling` (`idAccount`, `leveringsTijd`, `mededeling`, `prijs`) 
            VALUES (:idAccount, :leveringsTijd, :mededeling, :prijs)";


        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idAccount' => $idAccount,
            ':leveringsTijd' => $leveringsTijd,
            ':mededeling' => $mededeling,
            ':prijs' => $prijs,
        ));

        $laatsteId = $dbh->lastInsertId();

        if ($laatsteId > 0) {
            /* Update worked because query affected X amount of rows. */
            $result = $laatsteId;
        } else {
            $error = $dbh->errorInfo(); // Toont de DB-error
            $result = false;
            // TODO: exception omwille van fout bij creatie van bestelling 
        }

        // Verbreek connectie
        $dbh = null;

        // Return msg
        return $result;
    }

    public function update($idBestelling, $idAccount, $bestelTijd, $leveringsTijd, $mededeling, $prijs)
    {
        $sql =
            "UPDATE `aldofi1q_opleiding_pizza`.`bestelling` 
            SET `idAccount` = :idAccount, `bestelTijd` = :bestelTijd, `leveringsTijd` = :':leveringsTijd', `mededeling` = :mededeling, `prijs` = :prijs 
            WHERE (`idBestelling` = :idBestelling)";

        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':idBestelling' => $idBestelling,
            ':idAccount' => $idAccount,
            ':bestelTijd' => $bestelTijd,
            ':leveringsTijd' => $leveringsTijd,
            ':mededeling' => $mededeling,
            ':prijs' => $prijs,
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);

        $laatsteId = $dbh->lastInsertId();
        $aantalAangepast = $dbh->mysql_affected_rows();

        $dbh = null;

        if ($aantalAangepast > 0) {
            // update was geslaagd, stuur eventueel nieuwe id door
            $result =  $laatsteId;
        } else {
            // geen rij gevonden 
            $result =  null;
        };

        return $result;
    }
}
