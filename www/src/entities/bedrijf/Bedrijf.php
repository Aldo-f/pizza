<?php
namespace pizza\entities\bedrijf;

// INSERT INTO `aldofi1q_opleiding_pizza`.`bedrijf` (`bedrijfnaam`, `adres`, `huisnummer`, `telefoonnummer`, `idPlaats`) VALUES ('Domino\'s Overpoort', 'Overpoortstraat', '57', '092203737', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`bedrijf` (`bedrijfnaam`, `adres`, `huisnummer`, `telefoonnummer`, `idPlaats`) VALUES ('Domino\'s Gent-Sint-Pieters', 'Koningin Astridlaan', '226', '092259000', '1');


class Bedrijf
{
    //idBedrijf, bedrijfnaam, adres, huisnummer, telefoonnummer, idPlaats
    private static $idMap = [];

    protected $idBedrijf;
    protected $bedrijfnaam;
    protected $adres;
    protected $huisnummer;
    protected $telefoonnummer;
    protected $idPlaats;

    public function __construct($idBedrijf, $bedrijfnaam, $adres, $huisnummer, $telefoonnummer, $idPlaats)
    {
        $this->idBedrijf = $idBedrijf;
        $this->bedrijfnaam = $bedrijfnaam;
        $this->adres = $adres;
        $this->huisnummer = $huisnummer;
        $this->telefoonnummer = $telefoonnummer;
        $this->idPlaats = $idPlaats;
    }

    public static function create($idBedrijf, $bedrijfnaam, $adres, $huisnummer, $telefoonnummer, $idPlaats)
    {
        if (!isset(self::$idMap[$idBedrijf])) {
            self::$idMap[$idBedrijf] = new Bedrijf(
                $idBedrijf,
                $bedrijfnaam,
                $adres,
                $huisnummer,
                $telefoonnummer,
                $idPlaats
            );
        }
        return self::$idMap[$idBedrijf];
    }

    /**
     * Get the value of idBedrijf
     */
    public function getIdBedrijf()
    {
        return $this->idBedrijf;
    }

    /**
     * Set the value of idBedrijf
     *
     * @return  self
     */
    public function setIdBedrijf($idBedrijf)
    {
        $this->idBedrijf = $idBedrijf;

        return $this;
    }

    /**
     * Get the value of bedrijfnaam
     */
    public function getBedrijfnaam()
    {
        return $this->bedrijfnaam;
    }

    /**
     * Set the value of bedrijfnaam
     *
     * @return  self
     */
    public function setBedrijfnaam($bedrijfnaam)
    {
        $this->bedrijfnaam = $bedrijfnaam;

        return $this;
    }

    /**
     * Get the value of adres
     */
    public function getAdres()
    {
        return $this->adres;
    }

    /**
     * Set the value of adres
     *
     * @return  self
     */
    public function setAdres($adres)
    {
        $this->adres = $adres;

        return $this;
    }

    /**
     * Get the value of huisnummer
     */
    public function getHuisnummer()
    {
        return $this->huisnummer;
    }

    /**
     * Set the value of huisnummer
     *
     * @return  self
     */
    public function setHuisnummer($huisnummer)
    {
        $this->huisnummer = $huisnummer;

        return $this;
    }

    /**
     * Get the value of telefoonnummer
     */
    public function getTelefoonnummer()
    {
        return $this->telefoonnummer;
    }

    /**
     * Set the value of telefoonnummer
     *
     * @return  self
     */
    public function setTelefoonnummer($telefoonnummer)
    {
        $this->telefoonnummer = $telefoonnummer;

        return $this;
    }

    /**
     * Get the value of idPlaats
     */
    public function getIdPlaats()
    {
        return $this->idPlaats;
    }

    /**
     * Set the value of idPlaats
     *
     * @return  self
     */
    public function setIdPlaats($idPlaats)
    {
        $this->idPlaats = $idPlaats;

        return $this;
    }
}