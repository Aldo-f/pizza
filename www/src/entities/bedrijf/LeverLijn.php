<?php
namespace pizza\entities\bedrijf;

class LeverLijn
{
    // idBedrijf, idPlaats

    protected $idBedrijf;
    protected $idPlaats;

    public function __construct($idBedrijf, $idPlaats)
    {
        $this->idBedrijf = $idBedrijf;
        $this->idPlaats = $idPlaats;
    }

    public static function create($idBedrijf, $idPlaats)
    {
        $leverLijn = new LeverLijn(
            $idBedrijf,
            $idPlaats
        );
        return $leverLijn;
    }


    /**
     * Get the value of idPlaats
     */
    public function getIdPlaats()
    {
        return $this->idPlaats;
    }

    /**
     * Set the value of idPlaats
     *
     * @return  self
     */
    public function setIdPlaats($idPlaats)
    {
        $this->idPlaats = $idPlaats;

        return $this;
    }

    /**
     * Get the value of idBedrijf
     */
    public function getIdBedrijf()
    {
        return $this->idBedrijf;
    }

    /**
     * Set the value of idBedrijf
     *
     * @return  self
     */
    public function setIdBedrijf($idBedrijf)
    {
        $this->idBedrijf = $idBedrijf;

        return $this;
    }
}