<?php
namespace pizza\entities\product;

// INSERT INTO `aldofi1q_opleiding_pizza`.`bodem` (`idSamenstelling`, `prijs`, `naam`) VALUES ('5', '1', 'Classic');
// INSERT INTO `aldofi1q_opleiding_pizza`.`bodem` (`idSamenstelling`, `prijs`, `naam`) VALUES ('6', '2', 'Italian');
// INSERT INTO `aldofi1q_opleiding_pizza`.`bodem` (`idSamenstelling`, `prijs`, `naam`) VALUES ('7', '3', 'Cheesy Crust');


class Bodem
{
    // idBodem, idSamenstelling, prijs, naam
    private static $idMap = [];

    protected $idBodem;
    protected $idSamenstelling;
    protected $prijs;
    protected $naam;

    public function __construct($idBodem, $idSamenstelling, $prijs, $naam)
    {
        $this->idBodem = $idBodem;
        $this->idSamenstelling = $idSamenstelling;
        $this->prijs = $prijs;
        $this->naam = $naam;
    }

    public static function create($idBodem, $idSamenstelling, $prijs, $naam)
    {
        if (!isset(self::$idMap[$idBodem])) {
            self::$idMap[$idBodem] = new Bodem(
                $idBodem,
                $idSamenstelling,
                $prijs,
                $naam
            );
        }
        return self::$idMap[$idBodem];
    }

    /**
     * Get the value of idBodem
     */
    public function getIdBodem()
    {
        return $this->idBodem;
    }

    /**
     * Set the value of idBodem
     *
     * @return  self
     */
    public function setIdBodem($idBodem)
    {
        $this->idBodem = $idBodem;

        return $this;
    }

    /**
     * Get the value of idSamenstelling
     */
    public function getIdSamenstelling()
    {
        return $this->idSamenstelling;
    }

    /**
     * Set the value of idSamenstelling
     *
     * @return  self
     */
    public function setIdSamenstelling($idSamenstelling)
    {
        $this->idSamenstelling = $idSamenstelling;

        return $this;
    }

    /**
     * Get the value of prijs
     */
    public function getPrijs()
    {
        return $this->prijs;
    }

    /**
     * Set the value of prijs
     *
     * @return  self
     */
    public function setPrijs($prijs)
    {
        $this->prijs = $prijs;

        return $this;
    }

    /**
     * Get the value of naam
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Set the value of naam
     *
     * @return  self
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;

        return $this;
    }
}