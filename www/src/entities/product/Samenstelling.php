<?php
namespace pizza\entities\product;

// INSERT INTO `aldofi1q_opleiding_pizza`.`samenstelling` (`naam`, `categorie`) VALUES ('vlees', 'ingredient');
// INSERT INTO `aldofi1q_opleiding_pizza`.`samenstelling` (`naam`, `categorie`) VALUES ('kaas', 'bodem');
// INSERT INTO `aldofi1q_opleiding_pizza`.`samenstelling` (`naam`, `categorie`) VALUES ('kaas', 'ingredient');
// INSERT INTO `aldofi1q_opleiding_pizza`.`samenstelling` (`naam`, `categorie`) VALUES ('vis', 'ingredient');
// INSERT INTO `aldofi1q_opleiding_pizza`.`samenstelling` (`naam`, `categorie`) VALUES ('gluten', 'bodem');
// INSERT INTO `aldofi1q_opleiding_pizza`.`samenstelling` (`naam`, `categorie`) VALUES ('glutenvrij', 'bodem');
// INSERT INTO `aldofi1q_opleiding_pizza`.`samenstelling` (`naam`, `categorie`) VALUES ('lactose', 'saus');


class Samenstelling
{
    // idSamenstelling, naam, calorieën
    private static $idMap = [];

    protected $idSamenstelling;
    protected $naam;
    protected $calorieën;
    protected $categorie;

    public function __construct($idSamenstelling, $naam, $calorieën, $categorie)
    {
        $this->idSamenstelling = $idSamenstelling;
        $this->naam = $naam;
        $this->calorieën = $calorieën;
        $this->categorie = $categorie;
    }

    public static function create($idSamenstelling, $naam, $calorieën, $categorie)
    {
        if (!isset(self::$idMap[$idSamenstelling])) {
            self::$idMap[$idSamenstelling] = new Samenstelling(
                $idSamenstelling,
                $naam,
                $calorieën,
                $categorie
            );
        }
        return self::$idMap[$idSamenstelling];
    }

    /**
     * Get the value of idSamenstelling
     */
    public function getIdSamenstelling()
    {
        return $this->idSamenstelling;
    }

    /**
     * Set the value of idSamenstelling
     *
     * @return  self
     */
    public function setIdSamenstelling($idSamenstelling)
    {
        $this->idSamenstelling = $idSamenstelling;

        return $this;
    }

    /**
     * Get the value of naam
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Set the value of naam
     *
     * @return  self
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;

        return $this;
    }

    /**
     * Get the value of calorieën
     */
    public function getCalorieën()
    {
        return $this->calorieën;
    }

    /**
     * Set the value of calorieën
     *
     * @return  self
     */
    public function setCalorieën($calorieën)
    {
        $this->calorieën = $calorieën;

        return $this;
    }

    /**
     * Get the value of categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set the value of categorie
     *
     * @return  self
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }
}