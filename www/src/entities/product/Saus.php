<?php
namespace pizza\entities\product;

// INSERT INTO `aldofi1q_opleiding_pizza`.`saus` (`naam`, `prijs`) VALUES ('BBQ-saus', '1.5');
// INSERT INTO `aldofi1q_opleiding_pizza`.`saus` (`idSamenstelling`, `naam`, `prijs`) VALUES ('7', 'Crème Fraîche', '2');
// INSERT INTO `aldofi1q_opleiding_pizza`.`saus` (`naam`, `prijs`) VALUES ('Tomatensaus', '1');


class Saus
{
    // idSaus, idSamenstelling, naam, prijs
    private static $idMap = [];

    protected $idSaus;
    protected $idSamenstelling;
    protected $naam;
    protected $prijs;

    public function __construct($idSaus, $idSamenstelling, $naam, $prijs)
    {
        $this->idSaus = $idSaus;
        $this->idSamenstelling = $idSamenstelling;
        $this->naam = $naam;
        $this->prijs = $prijs;
    }

    public static function create($idSaus, $idSamenstelling, $naam, $prijs)
    {
        if (!isset(self::$idMap[$idSaus])) {
            self::$idMap[$idSaus] = new Saus(
                $idSaus,
                $idSamenstelling,
                $naam,
                $prijs
            );
        }
        return self::$idMap[$idSaus];
    }

    /**
     * Get the value of idSaus
     */
    public function getIdSaus()
    {
        return $this->idSaus;
    }

    /**
     * Set the value of idSaus
     *
     * @return  self
     */
    public function setIdSaus($idSaus)
    {
        $this->idSaus = $idSaus;

        return $this;
    }

    /**
     * Get the value of idSamenstelling
     */
    public function getIdSamenstelling()
    {
        return $this->idSamenstelling;
    }

    /**
     * Set the value of idSamenstelling
     *
     * @return  self
     */
    public function setIdSamenstelling($idSamenstelling)
    {
        $this->idSamenstelling = $idSamenstelling;

        return $this;
    }

    /**
     * Get the value of naam
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Set the value of naam
     *
     * @return  self
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;

        return $this;
    }

    /**
     * Get the value of prijs
     */
    public function getPrijs()
    {
        return $this->prijs;
    }

    /**
     * Set the value of prijs
     *
     * @return  self
     */
    public function setPrijs($prijs)
    {
        $this->prijs = $prijs;

        return $this;
    }
}