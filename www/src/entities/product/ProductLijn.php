<?php
namespace pizza\entities\product;

class ProductLijn
{
    // idBedrijf, idProduct, beschikbaar
    private static $idMap = [];

    protected $idProductLijn;
    protected $idBedrijf;
    protected $idProduct;
    protected $beschikbaar;

    public function __construct($idProductLijn, $idBedrijf, $idProduct, $beschikbaar)
    {
        $this->idProductLijn = $idProductLijn;
        $this->idBedrijf = $idBedrijf;
        $this->idProduct = $idProduct;
        $this->beschikbaar = $beschikbaar;
    }

    public static function create($idProductLijn, $idBedrijf, $idProduct, $beschikbaar)
    {
        $productLijn = new ProductLijn(
            $idProductLijn,
            $idBedrijf,
            $idProduct,
            $beschikbaar
        );
        return $productLijn;
    }

    /**
     * Get the value of idBedrijf
     */
    public function getIdBedrijf()
    {
        return $this->idBedrijf;
    }

    /**
     * Set the value of idBedrijf
     *
     * @return  self
     */
    public function setIdBedrijf($idBedrijf)
    {
        $this->idBedrijf = $idBedrijf;

        return $this;
    }

    /**
     * Get the value of idProduct
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set the value of idProduct
     *
     * @return  self
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get the value of beschikbaar
     */
    public function getBeschikbaar()
    {
        return $this->beschikbaar;
    }

    /**
     * Set the value of beschikbaar
     *
     * @return  self
     */
    public function setBeschikbaar($beschikbaar)
    {
        $this->beschikbaar = $beschikbaar;

        return $this;
    }

    /**
     * Get the value of idProductLijn
     */
    public function getIdProductLijn()
    {
        return $this->idProductLijn;
    }

    /**
     * Set the value of idProductLijn
     *
     * @return  self
     */
    public function setIdProductLijn($idProductLijn)
    {
        $this->idProductLijn = $idProductLijn;

        return $this;
    }
}