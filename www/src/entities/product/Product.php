<?php
namespace pizza\entities\product;

class Product
{
    // idProduct, naam, omschrijving, idFormaat, idBodem, idSaus
    private static $idMap = [];

    protected $idProduct;
    protected $naam;
    protected $omschrijving;
    protected $idFormaat;
    protected $idBodem;
    protected $idSaus;

    public function __construct($idProduct, $naam, $omschrijving, $idFormaat, $idBodem, $idSaus)
    {
        $this->idProduct = $idProduct;
        $this->naam = $naam;
        $this->omschrijving = $omschrijving;
        $this->idFormaat = $idFormaat;
        $this->idBodem = $idBodem;
        $this->idSaus = $idSaus;
    }

    public static function create($idProduct, $naam, $omschrijving, $idFormaat, $idBodem, $idSaus)
    {
        if (!isset(self::$idMap[$idProduct])) {
            self::$idMap[$idProduct] = new Product(
                $idProduct,
                $naam,
                $omschrijving,
                $idFormaat,
                $idBodem,
                $idSaus
            );
        }
        return self::$idMap[$idProduct];


        // $product = new Product(
        //     $idProduct,
        //     $naam,
        //     $omschrijving,
        //     $idFormaat,
        //     $idBodem,
        //     $idSaus
        // );

        // return $product;
    }


    /**
     * Get the value of idSaus
     */
    public function getIdSaus()
    {
        return $this->idSaus;
    }

    /**
     * Set the value of idSaus
     *
     * @return  self
     */
    public function setIdSaus($idSaus)
    {
        $this->idSaus = $idSaus;

        return $this;
    }

    /**
     * Get the value of idBodem
     */
    public function getIdBodem()
    {
        return $this->idBodem;
    }

    /**
     * Set the value of idBodem
     *
     * @return  self
     */
    public function setIdBodem($idBodem)
    {
        $this->idBodem = $idBodem;

        return $this;
    }

    /**
     * Get the value of idFormaat
     */
    public function getIdFormaat()
    {
        return $this->idFormaat;
    }

    /**
     * Set the value of idFormaat
     *
     * @return  self
     */
    public function setIdFormaat($idFormaat)
    {
        $this->idFormaat = $idFormaat;

        return $this;
    }

    /**
     * Get the value of omschrijving
     */
    public function getOmschrijving()
    {
        return $this->omschrijving;
    }

    /**
     * Set the value of omschrijving
     *
     * @return  self
     */
    public function setOmschrijving($omschrijving)
    {
        $this->omschrijving = $omschrijving;

        return $this;
    }

    /**
     * Get the value of naam
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Set the value of naam
     *
     * @return  self
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;

        return $this;
    }

    /**
     * Get the value of idProduct
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set the value of idProduct
     *
     * @return  self
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }
}