<?php
namespace pizza\entities\product;

// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('ananas', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('augurk', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('bacon', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('champignons', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('emmentaler', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('garnalen', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('gehakt', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('geitenkaas', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('gorgonzola', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('grilled veggie mix', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('ham', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('honing', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('kip kebap halal', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('kip kebap', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('lente-uitjes', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('mediterrane provence kruiden', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('merguez', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('mozzarella', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('paprika', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('pepperoni', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('reblochon', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('rode ui', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('ruindvleesstrips', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('spaanse pepers', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('spinazie', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('tonijn', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('vegan kaas', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('tomaat', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`ingredient` (`naam`, `prijs`) VALUES ('zwarte olijven', '1');


class Ingredient
{
    // idIngredient, idSamenstelling, naam, prijs
    private static $idMap = [];

    protected $idIngredient;
    protected $idSamenstelling;
    protected $naam;
    protected $prijs;

    public function __construct($idIngredient, $idSamenstelling, $naam, $prijs)
    {
        $this->idIngredient = $idIngredient;
        $this->idSamenstelling = $idSamenstelling;
        $this->naam = $naam;
        $this->prijs = $prijs;
    }

    public static function create($idIngredient, $idSamenstelling, $naam, $prijs)
    {
        if (!isset(self::$idMap[$idIngredient])) {
            self::$idMap[$idIngredient] = new Ingredient(
                $idIngredient,
                $idSamenstelling,
                $naam,
                $prijs
            );
        }
        return self::$idMap[$idIngredient];
    }

    /**
     * Get the value of idIngredient
     */
    public function getIdIngredient()
    {
        return $this->idIngredient;
    }

    /**
     * Set the value of idIngredient
     *
     * @return  self
     */
    public function setIdIngredient($idIngredient)
    {
        $this->idIngredient = $idIngredient;

        return $this;
    }

    /**
     * Get the value of idSamenstelling
     */
    public function getIdSamenstelling()
    {
        return $this->idSamenstelling;
    }

    /**
     * Set the value of idSamenstelling
     *
     * @return  self
     */
    public function setIdSamenstelling($idSamenstelling)
    {
        $this->idSamenstelling = $idSamenstelling;

        return $this;
    }

    /**
     * Get the value of naam
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Set the value of naam
     *
     * @return  self
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;

        return $this;
    }

    /**
     * Get the value of prijs
     */
    public function getPrijs()
    {
        return $this->prijs;
    }

    /**
     * Set the value of prijs
     *
     * @return  self
     */
    public function setPrijs($prijs)
    {
        $this->prijs = $prijs;

        return $this;
    }
}