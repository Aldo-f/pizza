<?php
namespace pizza\entities\product;

// OPGELET: in DB: IngridientLijn (met "i" i.p.v. "e")... 

class IngredientLijn
{
    // idIngredient, idProduct
    private static $idMap = [];

    protected $idIngredient;
    protected $idProduct;

    public function __construct($idIngredient, $idProduct)
    {
        $this->idIngredient = $idIngredient;
        $this->idProduct = $idProduct;
    }

    public static function create($idIngredient, $idProduct)
    {
        // if (!isset(self::$idMap[$idIngredient])) {
        //     self::$idMap[$idIngredient] = new IngredientLijn(
        //         $idIngredient,
        //         $idProduct
        //     );
        // }        
        // return self::$idMap[$idIngredient];

        $ingredientLijn = new IngredientLijn(
            $idIngredient,
            $idProduct
        );
        return $ingredientLijn;
    }

    /**
     * Get the value of idIngredient
     */
    public function getIdIngredient()
    {
        return $this->idIngredient;
    }

    /**
     * Set the value of idIngredient
     *
     * @return  self
     */
    public function setIdIngredient($idIngredient)
    {
        $this->idIngredient = $idIngredient;

        return $this;
    }

    /**
     * Get the value of idProduct
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set the value of idProduct
     *
     * @return  self
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }
}