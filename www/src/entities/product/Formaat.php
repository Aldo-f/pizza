<?php
namespace pizza\entities\product;

// INSERT INTO `aldofi1q_opleiding_pizza`.`formaat` (`naam`, `prijs`) VALUES ('Regular', '1');
// INSERT INTO `aldofi1q_opleiding_pizza`.`formaat` (`naam`, `prijs`) VALUES ('Medium', '2');
// INSERT INTO `aldofi1q_opleiding_pizza`.`formaat` (`naam`, `prijs`) VALUES ('Large', '3');
// INSERT INTO `aldofi1q_opleiding_pizza`.`formaat` (`naam`, `prijs`) VALUES ('XXL', '4');

class Formaat
{
    // idFormaat, idSamenstelling, naam, prijs
    private static $idMap = [];

    protected $idFormaat;
    protected $idSamenstelling;
    protected $naam;
    protected $prijs;


    public function __construct($idFormaat, $idSamenstelling, $naam, $prijs)
    {
        $this->idFormaat = $idFormaat;
        $this->idSamenstelling = $idSamenstelling;
        $this->naam = $naam;
        $this->prijs = $prijs;
    }


    public static function create($idFormaat, $idSamenstelling, $naam, $prijs)
    {
        if (!isset(self::$idMap[$idFormaat])) {
            self::$idMap[$idFormaat] = new Formaat(
                $idFormaat,
                $idSamenstelling,
                $naam,
                $prijs
            );
        }
        return self::$idMap[$idFormaat];
    }

    /**
     * Get the value of idFormaat
     */
    public function getIdFormaat()
    {
        return $this->idFormaat;
    }

    /**
     * Set the value of idFormaat
     *
     * @return  self
     */
    public function setIdFormaat($idFormaat)
    {
        $this->idFormaat = $idFormaat;

        return $this;
    }

    /**
     * Get the value of idSamenstelling
     */
    public function getIdSamenstelling()
    {
        return $this->idSamenstelling;
    }

    /**
     * Set the value of idSamenstelling
     *
     * @return  self
     */
    public function setIdSamenstelling($idSamenstelling)
    {
        $this->idSamenstelling = $idSamenstelling;

        return $this;
    }

    /**
     * Get the value of naam
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Set the value of naam
     *
     * @return  self
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;

        return $this;
    }

    /**
     * Get the value of prijs
     */
    public function getPrijs()
    {
        return $this->prijs;
    }

    /**
     * Set the value of prijs
     *
     * @return  self
     */
    public function setPrijs($prijs)
    {
        $this->prijs = $prijs;

        return $this;
    }
}