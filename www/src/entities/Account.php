<?php
namespace pizza\entities;


// idAccount, naam, voornaam, adres, huisnummer, telefoonnummer, email, wachtwoord, geboortedatum, idPlaats
class Account
{
    private static $idMap = [];

    protected $idAccount;
    protected $naam;
    protected $voornaam;
    protected $adres;
    protected $huisnummer;
    protected $telefoonnummer;
    protected $email;
    protected $wachtwoord;
    protected $geboortedatum;
    protected $idPlaats;

    public function __construct($idAccount, $naam, $voornaam, $adres, $huisnummer, $telefoonnummer, $email, $wachtwoord, $geboortedatum, $idPlaats)
    {
        $this->idAccount = $idAccount;
        $this->naam = $naam;
        $this->voornaam = $voornaam;
        $this->adres = $adres;
        $this->huisnummer = $huisnummer;
        $this->telefoonnummer = $telefoonnummer;
        $this->email = $email;
        $this->wachtwoord = $wachtwoord;
        $this->geboortedatum = $geboortedatum;
        $this->idPlaats = $idPlaats;
    }

    public static function create($idAccount, $naam, $voornaam, $adres, $huisnummer, $telefoonnummer, $email, $wachtwoord, $geboortedatum, $idPlaats)
    {
        if (!isset(self::$idMap[$idAccount])) {
            self::$idMap[$idAccount] = new Account(
                $idAccount,
                $naam,
                $voornaam,
                $adres,
                $huisnummer,
                $telefoonnummer,
                $email,
                $wachtwoord,
                $geboortedatum,
                $idPlaats
            );
        }
        return self::$idMap[$idAccount];
    }

    /**
     * Get the value of idAccount
     */
    public function getIdAccount()
    {
        return $this->idAccount;
    }

    /**
     * Set the value of idAccount
     *
     * @return  self
     */
    public function setIdAccount($idAccount)
    {
        $this->idAccount = $idAccount;

        return $this;
    }

    /**
     * Get the value of naam
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Set the value of naam
     *
     * @return  self
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;

        return $this;
    }

    /**
     * Get the value of voornaam
     */
    public function getVoornaam()
    {
        return $this->voornaam;
    }

    /**
     * Set the value of voornaam
     *
     * @return  self
     */
    public function setVoornaam($voornaam)
    {
        $this->voornaam = $voornaam;

        return $this;
    }

    /**
     * Get the value of adres
     */
    public function getAdres()
    {
        return $this->adres;
    }

    /**
     * Set the value of adres
     *
     * @return  self
     */
    public function setAdres($adres)
    {
        $this->adres = $adres;

        return $this;
    }

    /**
     * Get the value of huisnummer
     */
    public function getHuisnummer()
    {
        return $this->huisnummer;
    }

    /**
     * Set the value of huisnummer
     *
     * @return  self
     */
    public function setHuisnummer($huisnummer)
    {
        $this->huisnummer = $huisnummer;

        return $this;
    }

    /**
     * Get the value of telefoonnummer
     */
    public function getTelefoonnummer()
    {
        return $this->telefoonnummer;
    }

    /**
     * Set the value of telefoonnummer
     *
     * @return  self
     */
    public function setTelefoonnummer($telefoonnummer)
    {
        $this->telefoonnummer = $telefoonnummer;

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of wachtwoord
     */
    public function getWachtwoord()
    {
        return $this->wachtwoord;
    }

    /**
     * Set the value of wachtwoord
     *
     * @return  self
     */
    public function setWachtwoord($wachtwoord)
    {
        $this->wachtwoord = $wachtwoord;

        return $this;
    }

    /**
     * Get the value of geboortedatum
     */
    public function getGeboortedatum()
    {
        return $this->geboortedatum;
    }

    /**
     * Set the value of geboortedatum
     *
     * @return  self
     */
    public function setGeboortedatum($geboortedatum)
    {
        $this->geboortedatum = $geboortedatum;

        return $this;
    }

    /**
     * Get the value of idPlaats
     */
    public function getIdPlaats()
    {
        return $this->idPlaats;
    }

    /**
     * Set the value of idPlaats
     *
     * @return  self
     */
    public function setIdPlaats($idPlaats)
    {
        $this->idPlaats = $idPlaats;

        return $this;
    }
}