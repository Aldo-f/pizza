<?php
namespace pizza\entities\bestelling;

class BestellingLijn
{
    protected $idPromotieLijn;
    protected $idProductLijn;
    protected $idBestelling;
    protected $aantalProductLijn;
    protected $aantalPromotieLijn;

    public function __construct($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn)
    {
        $this->idPromotieLijn = $idPromotieLijn;
        $this->idProductLijn = $idProductLijn;
        $this->idBestelling = $idBestelling;
        $this->aantalProductLijn = $aantalProductLijn;
        $this->aantalPromotieLijn = $aantalPromotieLijn;
    }

    public static function create($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn)
    {
        $bestellingLijn = new BestellingLijn(
            $idPromotieLijn,
            $idProductLijn,
            $idBestelling,
            $aantalProductLijn,
            $aantalPromotieLijn
        );
        return $bestellingLijn;
    }

    /**
     * Get the value of idPromotieLijn
     */
    public function getIdPromotieLijn()
    {
        return $this->idPromotieLijn;
    }

    /**
     * Set the value of idPromotieLijn
     *
     * @return  self
     */
    public function setIdPromotieLijn($idPromotieLijn)
    {
        $this->idPromotieLijn = $idPromotieLijn;

        return $this;
    }

    /**
     * Get the value of idProductLijn
     */
    public function getIdProductLijn()
    {
        return $this->idProductLijn;
    }

    /**
     * Set the value of idProductLijn
     *
     * @return  self
     */
    public function setIdProductLijn($idProductLijn)
    {
        $this->idProductLijn = $idProductLijn;

        return $this;
    }

    /**
     * Get the value of idBestelling
     */
    public function getIdBestelling()
    {
        return $this->idBestelling;
    }

    /**
     * Set the value of idBestelling
     *
     * @return  self
     */
    public function setIdBestelling($idBestelling)
    {
        $this->idBestelling = $idBestelling;

        return $this;
    }

    /**
     * Get the value of aantalProductLijn
     */
    public function getAantalProductLijn()
    {
        return $this->aantalProductLijn;
    }

    /**
     * Set the value of aantalProductLijn
     *
     * @return  self
     */
    public function setAantalProductLijn($aantalProductLijn)
    {
        $this->aantalProductLijn = $aantalProductLijn;

        return $this;
    }

    /**
     * Get the value of aantalPromotieLijn
     */
    public function getAantalPromotieLijn()
    {
        return $this->aantalPromotieLijn;
    }

    /**
     * Set the value of aantalPromotieLijn
     *
     * @return  self
     */
    public function setAantalPromotieLijn($aantalPromotieLijn)
    {
        $this->aantalPromotieLijn = $aantalPromotieLijn;

        return $this;
    }
}