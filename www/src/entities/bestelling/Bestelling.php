<?php
namespace pizza\entities\bestelling;

class Bestelling
{
    // idBestelling, idAccount, bestelTijd, leveringsTijd, mededeling, idBedrijf
    private static $idMap = [];

    protected $idBestelling;
    protected $idAccount;
    protected $bestelTijd;
    protected $leveringsTijd;
    protected $mededeling;
    protected $prijs;

    public function __construct($idBestelling, $idAccount, $bestelTijd, $leveringsTijd, $mededeling, $prijs)
    {
        $this->idBestelling = $idBestelling;
        $this->idAccount = $idAccount;
        $this->bestelTijd = $bestelTijd;
        $this->leveringsTijd = $leveringsTijd;
        $this->mededeling = $mededeling;
        $this->prijs = $prijs;
    }

    public static function create($idBestelling, $idAccount, $bestelTijd, $leveringsTijd, $mededeling, $prijs)
    {
        if (!isset(self::$idMap[$idBestelling])) {
            self::$idMap[$idBestelling] = new Bestelling(
                $idBestelling,
                $idAccount,
                $bestelTijd,
                $leveringsTijd,
                $mededeling,
                $prijs
            );
        }
        return self::$idMap[$idBestelling];
    }

    /**
     * Get the value of idBestelling
     */
    public function getIdBestelling()
    {
        return $this->idBestelling;
    }

    /**
     * Set the value of idBestelling
     *
     * @return  self
     */
    public function setIdBestelling($idBestelling)
    {
        $this->idBestelling = $idBestelling;

        return $this;
    }

    /**
     * Get the value of idAccount
     */
    public function getIdAccount()
    {
        return $this->idAccount;
    }

    /**
     * Set the value of idAccount
     *
     * @return  self
     */
    public function setIdAccount($idAccount)
    {
        $this->idAccount = $idAccount;

        return $this;
    }

    /**
     * Get the value of bestelTijd
     */
    public function getBestelTijd()
    {
        return $this->bestelTijd;
    }

    /**
     * Set the value of bestelTijd
     *
     * @return  self
     */
    public function setBestelTijd($bestelTijd)
    {
        $this->bestelTijd = $bestelTijd;

        return $this;
    }

    /**
     * Get the value of leveringsTijd
     */
    public function getLeveringsTijd()
    {
        return $this->leveringsTijd;
    }

    /**
     * Set the value of leveringsTijd
     *
     * @return  self
     */
    public function setLeveringsTijd($leveringsTijd)
    {
        $this->leveringsTijd = $leveringsTijd;

        return $this;
    }

    /**
     * Get the value of mededeling
     */
    public function getMededeling()
    {
        return $this->mededeling;
    }

    /**
     * Set the value of mededeling
     *
     * @return  self
     */
    public function setMededeling($mededeling)
    {
        $this->mededeling = $mededeling;

        return $this;
    }

    /**
     * Get the value of prijs
     */
    public function getPrijs()
    {
        return $this->prijs;
    }

    /**
     * Set the value of prijs
     *
     * @return  self
     */
    public function setPrijs($prijs)
    {
        $this->prijs = $prijs;

        return $this;
    }
}