<?php
namespace pizza\entities;

class Plaats
{
    private static $idMap = [];

    protected $idPlaats;
    protected $postcode;
    protected $plaats;

    public function __construct($idPlaats, $postcode, $plaats)
    {
        $this->idPlaats = $idPlaats;
        $this->postcode = $postcode;
        $this->plaats = $plaats;
    }

    public static function create($idPlaats, $postcode, $plaats)
    {
        if (!isset(self::$idMap[$idPlaats])) {
            self::$idMap[$idPlaats] = new Plaats(
                $idPlaats,
                $postcode,
                $plaats
            );
        }
        return self::$idMap[$idPlaats];
    }

    /**
     * Get the value of plaats
     */
    public function getPlaats()
    {
        return $this->plaats;
    }

    /**
     * Set the value of plaats
     *
     * @return  self
     */
    public function setPlaats($plaats)
    {
        $this->plaats = $plaats;

        return $this;
    }

    /**
     * Get the value of postcode
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set the value of postcode
     *
     * @return  self
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get the value of idPlaats
     */
    public function getIdPlaats()
    {
        return $this->idPlaats;
    }

    /**
     * Set the value of idPlaats
     *
     * @return  self
     */
    public function setIdPlaats($idPlaats)
    {
        $this->idPlaats = $idPlaats;

        return $this;
    }
}