<?php

namespace pizza\functions\helpers;

use pizza\business\product\ProductService;
use pizza\business\product\ProductLijnService;

class WinkelmandProducten
{
    function maakProductLijst($idBedrijf)
    {
        // Alle producten van bedrijfId
        $productLijnSvc = new ProductLijnService;
        $productLijnByIdBedrijf = $productLijnSvc->getByIdBedrijf($idBedrijf);

        $productLijst = [];

        if ($productLijnByIdBedrijf != null) {
            foreach ($productLijnByIdBedrijf as $key => $value) {
                // Haal op uit value
                // $idBedrijf = $value->getIdBedrijf();
                $idProduct = $value->getIdProduct();
                // $beschikbaar = $value->getBeschikbaar();

                $product =  self::maakProduct($idProduct);
                array_push($productLijst, $product);
            }
           
        } return $productLijst;
    }
    
    function maakProductLijstBeschikbaar($idBedrijf)
    {
        // Alle producten van bedrijfId
        $productLijnSvc = new ProductLijnService;
        $productLijnByIdBedrijf = $productLijnSvc->getByIdBedrijfAvailable($idBedrijf);

        $productLijst = [];

        if ($productLijnByIdBedrijf != null) {
            foreach ($productLijnByIdBedrijf as $key => $value) {
                // Haal op uit value
                // $idBedrijf = $value->getIdBedrijf();
                $idProduct = $value->getIdProduct();
                // $beschikbaar = $value->getBeschikbaar();

                $product =  self::maakProduct($idProduct);
                array_push($productLijst, $product);
            }
           
        } return $productLijst;
    }

    public function maakProduct($idProduct)
    {
        // Maak product
        $productSvc = new ProductService;
        $nieuwProduct = $productSvc->getProductById($idProduct);

        // plaats in stdClass
        $product = (object)[
            "idProduct" => $idProduct,
            "product" => $nieuwProduct
        ];
        return $product;
    }
}
