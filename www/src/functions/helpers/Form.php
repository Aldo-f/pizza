<?php

namespace pizza\functions\helpers;

class Form
{
    function test_input($data)
    {
        $data = trim($data); // zonder whitespace in begin of einde
        $data = stripslashes($data); // geen \
        $data = htmlspecialchars($data); // geen speciale tekens
        return $data;
    }
}
