<?php
use pizza\business\AccountService;

if (isset($_POST["actie"]) && $_POST["actie"] == "loginAccount") {
    $valid = true;

    /// Haal gegevens op
    // Login
    if (isset($_POST["login"])) {
        if (isset($_POST["login"]["email"])) {
            $email = test_input2($_POST["login"]["email"]);
        }
        if (isset($_POST["login"]["wachtwoord"])) {
            $wachtwoord = test_input2($_POST["login"]["wachtwoord"]);
        } elseif (isset($_POST["login"]["wachtwoord1"])) {
            $wachtwoord = test_input2($_POST["login"]["wachtwoord1"]);
        }
    } else {
        $valid = false;
    }
    // Behoud email
    if (isset($_POST["onthoud"])) {
        $onthoud = test_input2($_POST["onthoud"]);

        if ($onthoud && isset($email)) {
            // Plaats email in cookie vars (voor 1 week )
            setcookie("email", $email, time() + 3600 * 24 * 7);
        } else {
            // Unset eventuele reeds aanwezig email cookie
            if (isset($_COOKIE["email"])) {
                unset($_COOKIE['email']);
                setcookie('email', '', time() - 3600);
            }
        }
    }

    // TODO: Extra contole van input


    if ($valid) {
        // Zoek account in database

        $accountSvc = new AccountService;
        $account = $accountSvc->getByEmailWachtwoord($email, $wachtwoord);

        if ($account) {
            // Account werd gevonden
            $voornaam = $account->getVoornaam();
            $msgSuccess["header"] = "Welkom $voornaam";
            array_push($msgSuccess["msg"], "Je bent succesvol ingelogd");

            // Plaats account in session vars
            $_SESSION["account"] = serialize($account);

            // Voor de navigatie
            $ingelogd = true;
            $netIngelogd = true;
        } else {
            // TODO: vervang door exception
            $msgError["header"] = "Login incorrect";
            array_push($msgError["msg"], "E-mailadres of wachtwoord was verkeerd");
        }
    }
}

function test_input2($data)
{
    $data = trim($data); // zonder whitespace in begin of einde
    $data = stripslashes($data); // geen \
    $data = htmlspecialchars($data); // geen speciale tekens
    return $data;
}
