<?php
/**
 * Maakt vars indien ze niet bestaan
 * dit voor geen undefined vars door te gevens aan twig
 */
foreach (array(
    'account',
    'postcode',
    'bedrijfId',
    'winkelmand',
    'productLijst',
    'accountById',
    'plaats',
    'mandProductLijst',
    'idBedrijfLijst',
    'geenBedrijfVoorPostcode',
    'keuzeBedrijfLijst',
    'idProduct',
    'product',
    'formaatLijst',
    'bodemLijst',
    'sausLijst',
    'geenWaarde'
) as $var) {
    if (!isset($$var)) $$var = null;
}
