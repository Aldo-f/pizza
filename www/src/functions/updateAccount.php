<?php

use pizza\business\PlaatsService;
use pizza\business\AccountService;

// je moet nog ingelogd zijn om aan te kunnnen passen
if (isset($account)) {
    $idAccount =  $account->getIdAccount();

    if (isset($_POST["actie"]) && $_POST["actie"] == "updateAccount") {
        $valid = true;
        //Haal alles op


        //Van login
        if (isset($_POST["login"])) {
            if (isset($_POST["login"]["email"])) {
                $email = test_input($_POST["login"]["email"]);
            }
            if (isset($_POST["login"]["wachtwoord1"])) {
                $wachtwoord1 = test_input($_POST["login"]["wachtwoord1"]);
            }
            if (isset($_POST["login"]["wachtwoord2"])) {
                $wachtwoord2 = test_input($_POST["login"]["wachtwoord2"]);
            }
            if (isset($_POST["login"]["telefoonnummer"])) {
                $telefoonnummer = test_input($_POST["login"]["telefoonnummer"]);
            }
            if (isset($_POST["login"]["geboortedatum"])) {
                $geboortedatum = test_input($_POST["login"]["geboortedatum"]);
            }
        }
        // Van adresgegevens
        if (isset($_POST["account"])) {
            if (isset($_POST["account"]["voornaam"])) {
                $voornaam = test_input($_POST["account"]["voornaam"]);
            }
            if (isset($_POST["account"]["naam"])) {
                $naam = test_input($_POST["account"]["naam"]);
            }
        }
        // Van leveringsadres
        if (isset($_POST["levering"])) {
            if (isset($_POST["levering"]["straat"])) {
                $straat = test_input($_POST["levering"]["straat"]);
            }
            if (isset($_POST["levering"]["nummer"])) {
                $nummer = test_input($_POST["levering"]["nummer"]);
            }
            if (isset($_POST["levering"]["gemeente"])) {
                $gemeente = test_input($_POST["levering"]["gemeente"]);
            }
            if (isset($_POST["levering"]["postcode"])) {
                $postcode = test_input($_POST["levering"]["postcode"]);
            }
        }
        // Voorwaarden
        if (isset($_POST["voorwaarden"])) {
            $voorwaarden = test_input($_POST["voorwaarden"]);
        }

        // DEBUG
        // $msgSuccess["header"] = "Dit gaf je in";
        // array_push($msgSuccess["msg"], $email);
        // array_push($msgSuccess["msg"], $wachtwoord1);
        // array_push($msgSuccess["msg"], $wachtwoord2);
        // array_push($msgSuccess["msg"], $telefoonnummer);
        // array_push($msgSuccess["msg"], $geboortedatum);
        // array_push($msgSuccess["msg"], $voornaam);
        // array_push($msgSuccess["msg"], $naam);
        // array_push($msgSuccess["msg"], $straat);
        // array_push($msgSuccess["msg"], $nummer);
        // array_push($msgSuccess["msg"], $gemeente);
        // array_push($msgSuccess["msg"], $postcode);

        // TODO: vervolledig controle
        if ($wachtwoord1 != $wachtwoord2) {
            $valid = false;
        }

        if ($valid) {
            /// Haal plaatsId op
            $plaatsSvc = new PlaatsService;
            $plaats = $plaatsSvc->getBy($postcode, $gemeente);

            // Indien $plaats niet bestaat; maak aan
            if (!$plaats) {
                // Bestaat de gemeente al, maar met andere postcode? 
                $plaats = $plaatsSvc->getByPlaats($gemeente);

                // We gaan er vanuit dat iedereen weet hoe men hun gemeente typt..., en creeren een nieuwe plaats
                if (!$plaats) {
                    $idPlaats = $plaatsSvc->create($postcode, $gemeente);
                } else {
                    // De gemeente bestond reeds, we halen id op
                    $idPlaats = $plaats->getIdPlaats();
                }
            } else {
                // $plaats werd opgehaald
                $idPlaats = $plaats->getIdPlaats();
            }

            // Update account
            $accountSvc = new AccountService;
            $idAccount = $accountSvc->update($idAccount, $naam, $voornaam, $straat, $nummer, $telefoonnummer, $email, $wachtwoord1, $geboortedatum, $idPlaats);

            if ($idAccount) {
                $msgSuccess["header"] =  "Account werd aangepast";
                array_push($msgSuccess["msg"], "<a href='index.php'>Bestel</a> vlug een pizza");

                // update $account
                $accountSvc = new AccountService;
                $account = $accountSvc->getByEmailWachtwoord($email, $wachtwoord1);
            }

            // } else {
            //     // TODO: Pas aan naar exception
            //     $msgError["header"] =  "Account kon niet gecreërd worden";
            //     array_push($msgError["msg"], "Je hebt reeds een account");
            // }
        }
    }
}
function test_input($data)
{
    $data = trim($data); // zonder whitespace in begin of einde
    $data = stripslashes($data); // geen \
    $data = htmlspecialchars($data); // geen speciale tekens
    return $data;
}