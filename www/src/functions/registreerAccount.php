<?php
use pizza\business\PlaatsService;
use pizza\business\AccountService;

if (isset($_POST["actie"]) && $_POST["actie"] == "registreerAccount") {
    $valid = true;
    //Haal alles op

    //Van login
    if (isset($_POST["login"])) {
        if (isset($_POST["login"]["email"])) {
            $email = test_input($_POST["login"]["email"]);
        } else {
            $valid = false;
        }
        if (isset($_POST["login"]["wachtwoord1"])) {
            $wachtwoord1 = test_input($_POST["login"]["wachtwoord1"]);
        }
        if (isset($_POST["login"]["wachtwoord2"])) {
            $wachtwoord2 = test_input($_POST["login"]["wachtwoord2"]);
        }
        if (isset($_POST["login"]["telefoonnummer"])) {
            $telefoonnummer = test_input($_POST["login"]["telefoonnummer"]);
        } else {
            $valid = false;
        }
        if (isset($_POST["login"]["geboortedatum"])) {
            $geboortedatum = test_input($_POST["login"]["geboortedatum"]);
        } else {
            $valid = false;
        }
    } else {
        $valid = false;
    }
    // Van adresgegevens
    if (isset($_POST["account"])) {
        if (isset($_POST["account"]["voornaam"])) {
            $voornaam = test_input($_POST["account"]["voornaam"]);
        }
        if (isset($_POST["account"]["naam"])) {
            $naam = test_input($_POST["account"]["naam"]);
        } else {
            $valid = false;
        }
    } else {
        $valid = false;
    }
    // Van leveringsadres
    if (isset($_POST["levering"])) {
        if (isset($_POST["levering"]["straat"])) {
            $straat = test_input($_POST["levering"]["straat"]);
        } else {
            $valid = false;
        }
        if (isset($_POST["levering"]["nummer"])) {
            $nummer = test_input($_POST["levering"]["nummer"]);
        } else {
            $valid = false;
        }
        if (isset($_POST["levering"]["gemeente"])) {
            $gemeente = test_input($_POST["levering"]["gemeente"]);
        } else {
            $valid = false;
        }
        if (isset($_POST["levering"]["postcode"])) {
            $postcode = test_input($_POST["levering"]["postcode"]);
        } else {
            $valid = false;
        }
    }
    // Voorwaarden
    if (isset($_POST["voorwaarden"])) {
        $voorwaarden = test_input($_POST["voorwaarden"]);
    } else {
        $valid = false;
    }

    // DEBUG
    $msgSuccess["header"] = "Dit gaf je in";
    array_push($msgSuccess["msg"], $email);
    array_push($msgSuccess["msg"], $wachtwoord1);
    array_push($msgSuccess["msg"], $wachtwoord2);
    array_push($msgSuccess["msg"], $telefoonnummer);
    array_push($msgSuccess["msg"], $geboortedatum);
    array_push($msgSuccess["msg"], $voornaam);
    array_push($msgSuccess["msg"], $naam);
    array_push($msgSuccess["msg"], $straat);
    array_push($msgSuccess["msg"], $nummer);
    array_push($msgSuccess["msg"], $gemeente);
    array_push($msgSuccess["msg"], $postcode);

    // TODO: vervolledig controle
    if ($wachtwoord1 != $wachtwoord2) {
        $valid = false;
    }

    if ($valid) {
        /// Maak account
        // indien nog geen account met $email bestaat   
        // niet meer nodig, controle staat bij creatie      
        // $accountSvc = new AccountService;
        // $idAccount = $accountSvc->getByEmail($email);

        // if (!$idAccount) {
        // indien er geen account is
        /// Haal plaatsId op
        $plaatsSvc = new PlaatsService;
        $plaats = $plaatsSvc->getBy($postcode, $gemeente);

        // TODO: Indien $plaats niet bestaat; maak aan
        if (!$plaats) {
            // Bestaat de gemeente al, maar met andere postcode? 
            $plaats = $plaatsSvc->getByPlaats($gemeente);

            // We gaan er vanuit dat iedereen weet hoe men hun gemeente typt..., en creeren een nieuwe plaats
            if (!$plaats) {
                $idPlaats = $plaatsSvc->create($postcode, $gemeente);
            } else {
                // De gemeente bestond reeds, we halen id op
                $idPlaats = $plaats->getIdPlaats();
            }
        } else {
            // $plaats werd opgehaald
            $idPlaats = $plaats->getIdPlaats();
        }

        // echo $plaats;

        // Maak account
        $accountSvc = new AccountService;
        $idAccount = $accountSvc->create($naam, $voornaam, $straat, $nummer, $telefoonnummer, $email, $wachtwoord1, $geboortedatum, $idPlaats);

        if ($idAccount) {
            $msgSuccess["header"] =  "Account werd gecreërd";
            array_push($msgSuccess["msg"], "<a href='index.php'>Bestel</a> vlug een pizza");
        }

        // } else {
        //     // TODO: Pas aan naar exception
        //     $msgError["header"] =  "Account kon niet gecreërd worden";
        //     array_push($msgError["msg"], "Je hebt reeds een account");
        // }
    }
}

function test_input($data)
{
    $data = trim($data); // zonder whitespace in begin of einde
    $data = stripslashes($data); // geen \
    $data = htmlspecialchars($data); // geen speciale tekens
    return $data;
}
