<?php
use pizza\business\PlaatsService;
use pizza\business\AccountService;

if (isset($_POST["actie"]) && $_POST["actie"] == "registreerAccountBestel") {
    $valid = true;
    $registratie = false;
    //Haal alles op

    //Van login
    // indien iets ingevuld gaan we er van uit dat persoon zich wilt registreren
    if (isset($_POST["login"])) {
        if (isset($_POST["login"]["email"])) {
            $email = test_input4($_POST["login"]["email"]);
            if ($email == "") {
                $email = null;
            } else {
                $registratie = true;                
            }
        }
        if (isset($_POST["login"]["wachtwoord1"])) {
            $wachtwoord1 = test_input4($_POST["login"]["wachtwoord1"]);
            if ($wachtwoord1 == "") {
                $wachtwoord1 = null;
            } else {
                $registratie = true;                
            }
        }
        if (isset($_POST["login"]["wachtwoord2"])) {
            $wachtwoord2 = test_input4($_POST["login"]["wachtwoord2"]);
            if ($wachtwoord2 == "") {
                $wachtwoord2 = null;
            } else {
                $registratie = true;                
            }
        }
        // end verplicht voor registratie
        if (isset($_POST["login"]["telefoonnummer"])) {
            $telefoonnummer = test_input4($_POST["login"]["telefoonnummer"]);
        } else {
            $valid = false;
        }
        if (isset($_POST["login"]["geboortedatum"])) {
            $geboortedatum = test_input4($_POST["login"]["geboortedatum"]);
        } else {
            $valid = false;
        }
    } else {
        $valid = false;
    }
    // Van adresgegevens
    if (isset($_POST["account"])) {
        if (isset($_POST["account"]["voornaam"])) {
            $voornaam = test_input4($_POST["account"]["voornaam"]);
        } else {
            $valid = false;
        }
        if (isset($_POST["account"]["naam"])) {
            $naam = test_input4($_POST["account"]["naam"]);
        } else {
            $valid = false;
        }
    } else {
        $valid = false;
    }
    // Van leveringsadres
    if (isset($_POST["levering"])) {
        if (isset($_POST["levering"]["straat"])) {
            $straat = test_input4($_POST["levering"]["straat"]);
        }
        if (isset($_POST["levering"]["nummer"])) {
            $nummer = test_input4($_POST["levering"]["nummer"]);
        }
        if (isset($_POST["levering"]["gemeente"])) {
            $gemeente = test_input4($_POST["levering"]["gemeente"]);
        }
        if (isset($_POST["levering"]["postcode"])) {
            $postcode = test_input4($_POST["levering"]["postcode"]);
        }
    }
    // Voorwaarden
    if (isset($_POST["voorwaarden"])) {
        $voorwaarden = test_input4($_POST["voorwaarden"]);
    } else {
        $valid = false;
    }

    // DEBUG
    // $msgSuccess["header"] = "Dit gaf je in";
    // array_push($msgSuccess["msg"], $email);
    // array_push($msgSuccess["msg"], $wachtwoord1);
    // array_push($msgSuccess["msg"], $wachtwoord2);
    // array_push($msgSuccess["msg"], $telefoonnummer);
    // array_push($msgSuccess["msg"], $geboortedatum);
    // array_push($msgSuccess["msg"], $voornaam);
    // array_push($msgSuccess["msg"], $naam);
    // array_push($msgSuccess["msg"], $straat);
    // array_push($msgSuccess["msg"], $nummer);
    // array_push($msgSuccess["msg"], $gemeente);
    // array_push($msgSuccess["msg"], $postcode);

    // TODO: vervolledig controle
    if ($registratie) {
        if (isset($wachtwoord1) && isset($wachtwoord2)) {
            if ($wachtwoord1 != $wachtwoord2) {
                $valid = false;
            }
        }
    }

    if ($valid) {
        /// Maak account
        // indien nog geen account met $email bestaat   
        // niet meer nodig, controle staat bij creatie   

        $plaatsSvc = new PlaatsService;
        $plaats = $plaatsSvc->getBy($postcode, $gemeente);

        // TODO: Indien $plaats niet bestaat; maak aan
        if (!$plaats) {
            // Bestaat de gemeente al, maar met andere postcode? 
            $plaats = $plaatsSvc->getByPlaats($gemeente);

            // We gaan er vanuit dat iedereen weet hoe men hun gemeente typt..., en creeren een nieuwe plaats
            if (!$plaats) {
                $idPlaats = $plaatsSvc->create($postcode, $gemeente);
            } else {
                // De gemeente bestond reeds, we halen id op
                $idPlaats = $plaats->getIdPlaats();
            }
        } else {
            // $plaats werd opgehaald
            $idPlaats = $plaats->getIdPlaats();
        }

        if (!$registratie) {
            // maak wachtwoord en email null
            $wachtwoord1;
            $email;
        }

        // Maak account
        $accountSvc = new AccountService;
        $idAccount = $accountSvc->create($naam, $voornaam, $straat, $nummer, $telefoonnummer, $email, $wachtwoord1, $geboortedatum, $idPlaats);

        if ($idAccount) {
            if ($registratie) {
                $msgSuccess["header"] =  "Account werd gecreërd";
                array_push($msgSuccess["msg"], "<a href='index.php'>Bestel</a> vlug een pizza");
            } else { }
        }
    }
}

function test_input4($data)
{
    $data = trim($data); // zonder whitespace in begin of einde
    $data = stripslashes($data); // geen \
    $data = htmlspecialchars($data); // geen speciale tekens
    return $data;
}
