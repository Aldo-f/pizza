<?php

use pizza\functions\helpers\WinkelmandProducten;
use pizza\business\bestelling\BestellingService;
use pizza\business\bestelling\BestellingLijnService;
use pizza\business\product\ProductLijnService;

if (isset($_POST["actie"]) && $_POST["actie"] == "plaatsBestelling") {

    

    $valid = true;
    //Van form
    if (isset($_POST["levertijd"])) {
        $leveringsTijd = test_input($_POST["levertijd"]);
        array_push($msgSuccess["msg"], "levertijd: $leveringsTijd");
        // controleer min 30 min
    } else {
        $valid = false;
        // of anders z.s.m.? 
    }
    if (isset($_POST["mededeling"])) {
        $mededeling = test_input($_POST["mededeling"]);
        if ($mededeling == "") {
            $mededeling = null;
            array_push($msgSuccess["msg"], "Geen mededeling");
        } else {
            array_push($msgSuccess["msg"], "mededeling: $mededeling");
        }
    } else {
        $mededeling;
    }

    if ($valid) {
        // laatste controle
        $ingelogd = false;

        if (isset($account)) {
            // ingelogd
            $idAccount =  $account->getIdAccount();
            if (isset($idAccount)) {
                // en idAccount gevonden
                $ingelogd = true;
                array_push($msgSuccess["msg"], "idAccount: $idAccount");
            }
        } 

        // prijs ophalen
        //maak productlijst van winkelmandje
        if (isset($winkelmand)) {
            if ($winkelmand->sessie->producten) {
                $idBedrijf = $winkelmand->sessie->bedrijfId;
                // het huidig geselcteerde bedrijf is deze van de sessie
                if (isset($idBedrijf)) {
                    $winkelSvc = new WinkelmandProducten;

                    $mandProductLijst = [];
                    foreach ($winkelmand->sessie->producten as $key => $idProduct) {
                        // echo "key:" . $key . ", idProduct: " . $idProduct;
                        $product = $winkelSvc->maakProduct($idProduct);
                        array_push($mandProductLijst, $product);
                    }
                    $aantalProducten = sizeof($mandProductLijst);

                    $prijs = 0;
                    $idProducten = [];
                    foreach ($mandProductLijst as $key => $value) {
                        array_push($idProducten, $value->idProduct);
                        $prijs += $value->product->prijs;
                    }

                    // maak enkel een bestelling indien er producten zijn, prijs kan eventueel 0 zijn
                    if ($aantalProducten > 0 && $prijs >= 0) {
                        array_push($msgSuccess["msg"], "aantal producten gekozen: $aantalProducten");

                        $bestellingSvc = new BestellingService;
                        $idBestelling = $bestellingSvc->create(
                            $idAccount,
                            $leveringsTijd,
                            $mededeling,
                            $prijs
                        );

                        // plaats producten bij bestelling
                        if (isset($idBestelling)) {
                            array_push($msgSuccess["msg"], "\$idBestelling: $idBestelling");

                            $bestellingsLijnSvc = new BestellingLijnService;
                            $productLijnSvc = new ProductLijnService;

                            // TODO: combineer gelijke id's en tel op tot aantalProductLijn
                            // is het aantal producten per bestellijn
                            $aantalProductLijn = 1;

                            $i = 0;
                            foreach ($idProducten as $idProduct) {

                                // maak productlijn
                                // 0 staat voor niet te vinden in a la carte
                                $idProductLijn = $productLijnSvc->create($idBedrijf, $idProduct, 0);

                                array_push($msgSuccess["msg"], "\$idProductLijn $idProductLijn");

                                // TODO: pas aantal aan bij bestellingsLijn op $idProductLijn
                                if ($idProductLijn) {
                                    if (!isset($idPromotieLijn)) {

                                        $bestellingsLijnSvc->createBasic(
                                            $idProductLijn,
                                            $idBestelling,
                                            $aantalProductLijn,
                                        );
                                    } else {

                                        $bestellingsLijnSvc->create(
                                            $idPromotieLijn,
                                            $idProductLijn,
                                            $idBestelling,
                                            $aantalProductLijn,
                                            $aantalPromotieLijn
                                        );
                                    }
                                    $i++;
                                }
                            }

                            $msgSuccess["header"] = "Bestelling werd succesvol geplaatst ";
                            if ($i > 1) {
                                array_push($msgSuccess["msg"], "Er werden $i producten besteld");
                            } else {
                                array_push($msgSuccess["msg"], "Er werd $i product besteld");
                            }
                            // verwijder winkelmand
                        } else {
                            // error via exception?
                        }
                    } else {
                        $msgError = geenItems();
                        unset($msgWarning);
                    }
                }
            } else {
                $msgError = geenItems();
                unset($msgWarning);
            }
        } else {
            $msgError = geenItems();
            unset($msgWarning);
        }
    } else {   
        $msgSuccess["msg"] = [];
    }
}

function geenItems()
{
    $msgError["msg"] = [];
    $msgError["header"] = "Niets te bestellen? ";
    array_push($msgError["msg"], "We konden je bestelling niet voltooien, want je hebt geen producten gekozen? ¯\_(ツ)_/¯");
    return $msgError;
}