<?php
use pizza\functions\helpers\Form;
use pizza\business\product\SausService;
use pizza\business\product\BodemService;
use pizza\business\product\FormaatService;
use pizza\business\product\ProductService;
use pizza\business\product\IngredientLijnService;

if (isset($_POST["actie"]["main"]) && $_POST["actie"]["main"] == "winkelmand") {

    if (isset($_POST["actie"]["sub"]) && $_POST["actie"]["sub"] == "updateProduct") {
        if (isset($_POST["idProduct"])) {
            $idProduct = $_POST["idProduct"];

            // haal volledig product op (voor in form)
            $productSvc = new ProductService;
            $product = $productSvc->getProductById($idProduct);

            // haal keuzes op
            $formaatSvc = new FormaatService;
            $formaatLijst = $formaatSvc->getAll();

            $bodemSvc = new BodemService;
            $bodemLijst = $bodemSvc->getAll();

            $sausSvc = new SausService;
            $sausLijst = $sausSvc->getAll();
        }
    }
}

if (isset($_POST["actie"]) && $_POST["actie"] == "nieuwProduct") {
    $form = new Form;

    if (isset($_POST["idProduct"])) {
        $vorigIdProduct = $form->test_input($_POST["idProduct"]);
    }
    if (isset($_POST["idFormaat"])) {
        $idFormaat = $form->test_input($_POST["idFormaat"]);
    }
    if (isset($_POST["idBodem"])) {
        $idBodem = $form->test_input($_POST["idBodem"]);
    }
    if (isset($_POST["idSaus"])) {
        $idSaus = $form->test_input($_POST["idSaus"]);
    }



    // controleer of verschil met vorig product    
    $productSvc = new ProductService;
    $product = $productSvc->getProductById($vorigIdProduct);

    $vorigIdFormaat = $product->product->getIdFormaat();
    $vorigIdBodem = $product->product->getIdBodem();
    $vorigIdSaus = $product->product->getIdSaus();

    $pasFormaatAan = ($idFormaat != $vorigIdFormaat) ? true : false;
    $pasBodemAan = ($idBodem != $vorigIdBodem) ? true : false;
    $pasSausAan = ($idSaus != $vorigIdSaus) ? true : false;

    if ($product && ($pasFormaatAan || $pasBodemAan || $pasSausAan)) {

        $idProduct = $productSvc->create(
            $product->product->getNaam(),
            $product->product->getOmschrijving(),
            $idFormaat,
            $idBodem,
            $idSaus
        );

        // maak de ingredientlijn hetzelfde als $product
        $ingredientIdLijst = [];

        $ingredientLijnSvc = new IngredientLijnService;
        $ingredientLijst = $ingredientLijnSvc->getByIdProduct($vorigIdProduct);

        foreach ($ingredientLijst as $key => $value) {
            // echo $value->getIdIngredient();
            array_push($ingredientIdLijst, $value->getIdIngredient());
        }

        // maak nieuwe ingredientlijst (van $ingredientIdLijst) voor nieuw $idProduct
        foreach ($ingredientIdLijst as $idIngredient) {
            // echo $value . " "; 
            $ingredientLijnSvc->create($idIngredient, $idProduct);
        }

        // haal volledige product op
        if ($idProduct) {
            $product = $productSvc->getProductById($idProduct);

            // vervang $vorigeIdProduct met $idProduct in winkelmandje             
            $vervangProduct = true;
            $oudProduct = $vorigIdProduct;
            $nieuwProduct = $idProduct;           


            $msgSuccess["header"] = "Pizza aangepast";
            array_push($msgSuccess["msg"],"En winkelmandje geüpdatet");
            array_push($msgSuccess["msg"], "Keer terug naar het <a href='index.php'>overzicht</a>, of <a href='bestel.php'>voltooi je bestelling</a>");
           
        }
    }

    // if ($pasIngredientenAan) {}



    // haal keuzes op
    $formaatSvc = new FormaatService;
    $formaatLijst = $formaatSvc->getAll();

    $bodemSvc = new BodemService;
    $bodemLijst = $bodemSvc->getAll();

    $sausSvc = new SausService;
    $sausLijst = $sausSvc->getAll();
}
