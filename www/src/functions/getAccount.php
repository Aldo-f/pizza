<?php

use pizza\business\PlaatsService;

if (isset($account)) {

    // moet eigenlijk niet opgehaald worden
    // $idAccount = $account->getIdAccount();
    // $accountSvc = new AccountService;
    // $accountById = $accountSvc->getById($idAccount);

    if (isset($accountById)) {
        $account = $accountById;

        $idPlaats = $account->getIdPlaats();
        $plaatsSvc = new PlaatsService;
        $plaats = $plaatsSvc->getById($idPlaats);
    } elseif (isset($account)) {
        $idPlaats = $account->getIdPlaats();
        $plaatsSvc = new PlaatsService;
        $plaats = $plaatsSvc->getById($idPlaats);
    }

    // TODO: unhash wachtwoord 
} else {
    $link = $_SERVER['PHP_SELF'];
    $link_array = explode('/',$link);
    $page = end($link_array);

    if ($page != "bestel.php") {
        $msgError["header"] = "Niet ingelogd";
        array_push($msgError["msg"], "Je moet ingelogd zijn om je account te kunnen aanpassen ;)");
    }
}
