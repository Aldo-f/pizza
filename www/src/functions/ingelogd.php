<?php

//  Controleer of gebruiker niet op 'logout' klikte
if (isset($_GET['actie']) && $_GET['actie'] == "logout") {
    // unset Gebruiker
    unset($_SESSION['account']);
    $ingelogd = false;

    $msgSuccess["header"] = "Successvol uitgelogd";
    array_push($msgSuccess["msg"], "U hebt zich successvol uitgelogd");
} else {
    // Indien reeds ingelogd?
    if (isset($_SESSION['account'])) {
        $account = unserialize($_SESSION['account']);

        // DEBUG
        // $voornaam =  $account->getVoornaam();
        // array_push($msgSuccess["msg"], "Uit sessie: $voornaam");

        $ingelogd = true;
    } else {
        // niet ingelogd
        $ingelogd = false;
    }
}