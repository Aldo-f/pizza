<?php

use pizza\business\PlaatsService;
use pizza\business\bedrijf\LeverLijnService;

if (isset($_POST["actie"]["main"]) && $_POST["actie"]["main"] == "winkelmand") {

    if (isset($_POST["actie"]["sub"]) && $_POST["actie"]["sub"] == "kiesPostcode") {
        if (isset($_POST["postcode"])) {
            $postcode = $_POST["postcode"];
            winkelmand(null, null, $postcode);
        }
    }
    if (isset($_POST["actie"]["sub"]) && $_POST["actie"]["sub"] == "kiesBedrijf") {
        if (isset($_POST["bedrijfId"])) {
            $idBedrijf = $_POST["bedrijfId"];

            // controleer of idBedrijf mogelijk is volgens postcode
            if (!isset($postcode)) {
                //Haal alles op van cookie
                if ($ingelogd) {
                    $winkelmand = winkelmand($ingelogd, $account);
                } else {
                    $winkelmand = winkelmand($ingelogd);
                }
                $postcode = $winkelmand->sessie->postcode;
            }

            $leverLijnSvc = new LeverLijnService;
            $leverLijnLijst = $leverLijnSvc->getByPostcode($postcode);
            $idBedrijfLijst = [];
            foreach ($leverLijnLijst as $key) {
                array_push($idBedrijfLijst, $key->getIdBedrijf());
            }

            $key =  array_search($idBedrijf, $idBedrijfLijst);

            if ($key !== false) {
                // komt voor, plaats in sessievars
                winkelmand(null, null, null, $idBedrijf);
            } else {
                // bedrijf komt niet voor in die postcode
                $msgError["header"] = "Fout input";
                array_push($msgError["msg"], "Gekozen bedrijf levert niet in jouw postcode");
            }
        }
    }
    if (isset($_POST["actie"]["sub"]) && $_POST["actie"]["sub"] == "kiesProduct") {
        if (isset($_POST["idProduct"])) {
            $nieuwIdProduct = $_POST["idProduct"];
            winkelmand(null, null, null, null, $nieuwIdProduct);
        }
    }
    if (isset($_POST["actie"]["sub"]) && $_POST["actie"]["sub"] == "verwijderProduct") {
        if (isset($_POST["idProduct"])) {
            $oudIdProduct = $_POST["idProduct"];
            // verwijder
            winkelmand(null, null, null, null, null, $oudIdProduct);
        }
    }
}


// vervang oud $idProduct met nieuw $idProduct indien in winkelmand aanwezig was
if (isset($vervangProduct) && $vervangProduct) {
    $oudProduct;
    $nieuwProduct;    

    winkelmand(null, null, null, null, $nieuwProduct, $oudProduct);

}

//Haal alles op van cookie
if ($ingelogd) {
    $winkelmand = winkelmand($ingelogd, $account);
} else {
    $winkelmand = winkelmand($ingelogd);
}
// $winkelmand = winkelmand($ingelogd, $account, null, null, null, null, null, false);

// Alles van account
if (isset($_SESSION['account'])) {
    $account = unserialize($_SESSION['account']);

    // Overschijf postcode met account info?
    $voornaam =  $account->getVoornaam();
    $idPlaats = $account->getIdPlaats();
    $plaatsSvc = new PlaatsService;
    $accountPostcode = ($plaatsSvc->getById($idPlaats))->getPostcode();
    $sessiePostcode = $winkelmand->sessie->postcode;
    if ($accountPostcode != $sessiePostcode) {
        // postcodes komen niet overeen
        $msgWarning["header"] = "Postcode komt niet overeen met account";
        array_push($msgWarning["msg"], "De postcode van je account ($accountPostcode) komt niet overeen met je gekozen postcode");
        array_push($msgWarning["msg"], "Wil je je <a href='mijnaccount.php'>account aanpassen</a>?");

        $postcode = $sessiePostcode;
    }
} else {
    // haal postcode uit $winkelmand
    // $postcode = $winkelmand->postcode;
    $postcode = $winkelmand->sessie->postcode;
}


function winkelmand($ingelogd = null, $account = null, $nieuwePostcode = null, $nieuwBedrijfId = null, $nieuwProduct = null, $oudProduct = null, $maak = true)
{
    // Cookie
    if (isset($_COOKIE["winkelmandCookie"])) {
        // haal uit cookie
        $winkelmandCookie = json_decode($_COOKIE["winkelmandCookie"]);
        // $winkelmand = unserialize($_COOKIE["winkelmand"]);
        $postcode = $winkelmandCookie->postcode;
        $bedrijfId = $winkelmandCookie->bedrijfId;
        $producten = $winkelmandCookie->producten;
        //plaats
        if ($nieuwePostcode) {
            $postcode = $nieuwePostcode;
        }
        if ($nieuwBedrijfId) {
            $bedrijfId = $nieuwBedrijfId;
        }
        if ($nieuwProduct) {
            array_push($producten, $nieuwProduct);
        }
    } else {
        // Indien ingelogd
        if ($ingelogd) {
            if (!isset($postcode)) {
                $idPlaats = $account->getIdPlaats();
                $plaatsSvc = new PlaatsService;
                $postcode = ($plaatsSvc->getById($idPlaats))->getPostcode();
            }
        } else {
            if ($nieuwePostcode) {
                $postcode = $nieuwePostcode;
            } else {
                $postcode = null;
            }
        }
        //plaats
        if ($nieuwBedrijfId) {
            $bedrijfId = $nieuwBedrijfId;
        } else {
            $bedrijfId = null;
        }
        $producten = [];
        if ($nieuwProduct) {
            array_push($producten, $nieuwProduct);
        }
    }
    // vul aan of maak
    $winkelmandCookie = (object)[
        "postcode" => $postcode,
        "bedrijfId" => $bedrijfId,
        "producten" => $producten
    ];

    if ($maak) {
        setcookie("winkelmandCookie", json_encode($winkelmandCookie), time() + 3600 * 24 * 7, "/");
    }

    // Sessie
    if (isset($_SESSION)) {

        if (isset($_SESSION['winkelmandSessie'])) {
            $winkelmandSessie = unserialize($_SESSION['winkelmandSessie']);
            //vul aan
            $postcode = $winkelmandSessie->postcode;
            $bedrijfId = $winkelmandSessie->bedrijfId;
            $producten = $winkelmandSessie->producten;

            // verwijder
            if (isset($oudProduct)) {
                $key = array_search($oudProduct, $producten);
                unset($producten[$key]);
            }
            //plaats
            if ($nieuwePostcode) {
                $postcode = $nieuwePostcode;
            }
            if ($nieuwBedrijfId) {
                $bedrijfId = $nieuwBedrijfId;

                // als nieuw bedrijf reeds in winkelmand zit
                // TODO: methode 1: plaats producten onder bedrijf, en haal ze dan op in functie van bedrijf

                // methode 2: verwijder de producten bij zetten van nieuw bedrijfId
                $producten = [];
            }
            if ($nieuwProduct) {
                array_push($producten, $nieuwProduct);
            }

            // maak
            $winkelmandSessie = (object)[
                "postcode" => $postcode,
                "bedrijfId" => $bedrijfId,
                "producten" => $producten
            ];
        } else {
            //plaats
            if ($ingelogd) {
                if (!isset($postcode)) {
                    $idPlaats = $account->getIdPlaats();
                    $plaatsSvc = new PlaatsService;
                    $postcode = ($plaatsSvc->getById($idPlaats))->getPostcode();
                }
            } else {
                if ($nieuwePostcode) {
                    $postcode = $nieuwePostcode;
                } elseif (!isset($postcode)) {
                    $postcode = null;
                }
            }
            if ($nieuwBedrijfId) {
                $bedrijfId = $nieuwBedrijfId;
            }
            $producten = [];
            if ($nieuwProduct) {
                array_push($producten, $nieuwProduct);
            }
            // maak
            $winkelmandSessie = (object)[
                "postcode" => $postcode,
                "bedrijfId" => $bedrijfId,
                "producten" => $producten
            ];
        }
        if ($maak) {
            $_SESSION["winkelmandSessie"] = serialize($winkelmandSessie);
        }
    }

    $winkelmand = (object)[
        "cookie" => $winkelmandCookie,
        "sessie" => $winkelmandSessie
    ];

    // delete cookie
    // cookie_delete("winkelmand");

    // geef de gedecodeerde door
    return $winkelmand;
}

function cookie_value($key)
{
    if (isset($_COOKIE[$key])) {
        $value =  $_COOKIE[$key];
    }
    return $value;
}

function cookie_delete($key)
{
    if (isset($_COOKIE[$key])) {
        unset($_COOKIE[$key]);
        setcookie($key, '', time() - 3600);
    }
}

function test_input($data)
{
    $data = trim($data); // zonder whitespace in begin of einde
    $data = stripslashes($data); // geen \
    $data = htmlspecialchars($data); // geen speciale tekens
    return $data;
}
