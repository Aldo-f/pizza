<?php
namespace pizza\business\product;

use pizza\data\product\SausDAO;

class SausService
{
    public function getAll()
    {
        $DAO = new SausDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getById($id)
    {
        $DAO = new SausDAO();
        $lijst = $DAO->getById($id);
        return $lijst;
    }

    public function create($idSamenstelling, $naam, $prijs)
    {
        $DAO = new SausDAO();
        $lijst = $DAO->create($idSamenstelling, $naam, $prijs);
        return $lijst;
    }

    public function update($idSaus, $idSamenstelling, $naam, $prijs)
    {
        $DAO = new SausDAO();
        $lijst = $DAO->update($idSaus, $idSamenstelling, $naam, $prijs);
        return $lijst;
    }
}