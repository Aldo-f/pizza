<?php
namespace pizza\business\product;

use pizza\data\product\IngredientLijnDAO;

// OPGELET: in DB: IngridientLijn (met "i" i.p.v. "e")... 

class IngredientLijnService
{
    public function getAll()
    {
        $DAO = new IngredientLijnDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getByIdProduct($id)
    {
        $DAO = new IngredientLijnDAO();
        $lijst = $DAO->getByIdProduct($id);
        return $lijst;
    }

    public function getByIdIngredient($id)
    {
        $DAO = new IngredientLijnDAO();
        $lijst = $DAO->getByIdIngredient($id);
        return $lijst;
    }

    public function create($idIngredient, $idProduct)
    {
        $DAO = new IngredientLijnDAO();
        $lijst = $DAO->create($idIngredient, $idProduct);
        return $lijst;
    }

    public function updateByProductId($idProduct, $idIngredient)
    {
        $DAO = new IngredientLijnDAO();
        $lijst = $DAO->updateByProductId($idProduct, $idIngredient);
        return $lijst;
    }

    public function updateByIngredientId($idIngredient, $idProduct)
    {
        $DAO = new IngredientLijnDAO();
        $lijst = $DAO->updateByIngredientId($idIngredient, $idProduct);
        return $lijst;
    }
}