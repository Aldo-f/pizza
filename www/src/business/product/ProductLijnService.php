<?php
namespace pizza\business\product;

use pizza\data\product\ProductLijnDAO;


class ProductLijnService
{
    public function getAll()
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }
    
    public function getAllAvailable()
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->getAllAvailable();
        return $lijst;
    }

    public function getById($id)
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->getById($id);
        return $lijst;
    }

    public function getByIdBedrijf($id)
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->getByIdBedrijf($id);
        return $lijst;
    }

    public function getByIdBedrijfAvailable($id)
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->getByIdBedrijfAvailable($id);
        return $lijst;
    }

    public function getByIdProduct($id)
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->getByIdProduct($id);
        return $lijst;
    }

    public function getByIdProductAndIdBedrijf($idProduct, $idBedrijf)
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->getByIdProductAndIdBedrijf($idProduct, $idBedrijf);
        return $lijst;
    }

    public function create($idBedrijf, $idProduct, $beschikbaar)
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->create($idBedrijf, $idProduct, $beschikbaar);
        return $lijst;
    }

    public function update($idProductLijn, $idBedrijf, $idProduct, $beschikbaar)
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->update($idProductLijn, $idBedrijf, $idProduct, $beschikbaar);
        return $lijst;
    }

    public function updateByProductId($idBedrijf, $idProduct, $beschikbaar)
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->updateByProductId($idBedrijf, $idProduct, $beschikbaar);
        return $lijst;
    }

    public function updateByBedrijfId($idBedrijf, $idProduct, $beschikbaar)
    {
        $DAO = new ProductLijnDAO();
        $lijst = $DAO->updateByBedrijfId($idBedrijf, $idProduct, $beschikbaar);
        return $lijst;
    }
}