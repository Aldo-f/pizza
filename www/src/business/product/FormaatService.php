<?php
namespace pizza\business\product;

use pizza\data\product\FormaatDAO;


class FormaatService
{
    public function getAll()
    {
        $DAO = new FormaatDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getById($id)
    {
        $DAO = new FormaatDAO();
        $lijst = $DAO->getById($id);
        return $lijst;
    }

    public function create($idSamenstelling, $naam, $prijs)
    {
        $DAO = new FormaatDAO();
        $lijst = $DAO->create($idSamenstelling, $naam, $prijs);
        return $lijst;
    }

    public function update($idFormaat, $idSamenstelling, $naam, $prijs)
    {
        $DAO = new FormaatDAO();
        $lijst = $DAO->update($idFormaat, $idSamenstelling, $naam, $prijs);
        return $lijst;
    }
}