<?php
namespace pizza\business\product;

use pizza\data\product\IngredientDAO;

class IngredientService
{
    public function getAll()
    {
        $DAO = new IngredientDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getById($id)
    {
        $DAO = new IngredientDAO();
        $lijst = $DAO->getById($id);
        return $lijst;
    }

    public function create($idSamenstelling, $naam, $prijs)
    {
        $DAO = new IngredientDAO();
        $lijst = $DAO->create($idSamenstelling, $naam, $prijs);
        return $lijst;
    }

    public function update($idIngredient, $idSamenstelling, $naam, $prijs)
    {
        $DAO = new IngredientDAO();
        $lijst = $DAO->update($idIngredient, $idSamenstelling, $naam, $prijs);
        return $lijst;
    }
}