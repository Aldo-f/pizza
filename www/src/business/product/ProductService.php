<?php
namespace pizza\business\product;

use pizza\data\product\ProductDAO;


class ProductService
{
    public function getAll()
    {
        $DAO = new ProductDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getById($id)
    {
        $DAO = new ProductDAO();
        $lijst = $DAO->getById($id);
        return $lijst;
    }

    public function getProductById($id)
    {
        $DAO = new ProductDAO();
        $lijst = $DAO->getProductById($id);
        return $lijst;
    }

    public function create($naam, $omschrijving, $idFormaat, $idBodem, $idSaus)
    {
        $DAO = new ProductDAO();
        $lijst = $DAO->create($naam, $omschrijving, $idFormaat, $idBodem, $idSaus);
        return $lijst;
    }

    public function update($idProduct, $naam, $omschrijving, $idFormaat, $idBodem, $idSaus)
    {
        $DAO = new ProductDAO();
        $lijst = $DAO->update($idProduct, $naam, $omschrijving, $idFormaat, $idBodem, $idSaus);
        return $lijst;
    }
}