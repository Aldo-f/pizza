<?php
namespace pizza\business\product;

use pizza\data\product\BodemDAO;

class BodemService
{
    public function getAll()
    {
        $DAO = new BodemDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getById($id)
    {
        $DAO = new BodemDAO();
        $lijst = $DAO->getById($id);
        return $lijst;
    }

    public function create($idSamenstelling, $naam, $prijs)
    {
        $DAO = new BodemDAO();
        $lijst = $DAO->create($idSamenstelling, $naam, $prijs);
        return $lijst;
    }

    public function update($idFormaat, $idSamenstelling, $naam, $prijs)
    {
        $DAO = new BodemDAO();
        $lijst = $DAO->update($idFormaat, $idSamenstelling, $naam, $prijs);
        return $lijst;
    }
}