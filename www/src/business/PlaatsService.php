<?php
namespace pizza\business;

use pizza\data\PlaatsDAO;

class PlaatsService
{
    public function getAll()
    {
        $DAO = new PlaatsDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getById($id)
    {
        $DAO = new PlaatsDAO();
        $lijst = $DAO->getById($id);
        return $lijst;
    }

    public function getBy($postcode, $plaats)
    {
        $DAO = new PlaatsDAO();
        $lijst = $DAO->getBy($postcode, $plaats);
        return $lijst;
    }
    public function getByPlaats($plaats)
    {
        $DAO = new PlaatsDAO();
        $lijst = $DAO->getByPlaats($plaats);
        return $lijst;
    }

    public function create($postcode, $plaats)
    {
        $DAO = new PlaatsDAO();
        $lijst = $DAO->create($postcode, $plaats);
        return $lijst;
    }
}