<?php
namespace pizza\business\bestelling;

use pizza\data\bestelling\BestellingDAO;

class BestellingService
{
    public function getAll()
    {
        $DAO = new BestellingDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getBasicAll()
    {
        $DAO = new BestellingDAO();
        $lijst = $DAO->getBasicAll();
        return $lijst;
    }

    public function getById($id)
    {
        $DAO = new BestellingDAO();
        $lijst = $DAO->getById($id);
        return $lijst;
    }
    
    public function getBasicById($id)
    {
        $DAO = new BestellingDAO();
        $lijst = $DAO->getBasicById($id);
        return $lijst;
    }

    public function getByIdAccount($id)
    {
        $DAO = new BestellingDAO();
        $lijst = $DAO->getByIdAccount($id);
        return $lijst;
    }

    public function getBasicByIdAccount($id)
    {
        $DAO = new BestellingDAO();
        $lijst = $DAO->getBasicByIdAccount($id);
        return $lijst;
    }

    public function create($idAccount, $leveringsTijd, $mededeling, $prijs)
    {
        $DAO = new BestellingDAO();
        $lijst = $DAO->create($idAccount, $leveringsTijd, $mededeling, $prijs);
        return $lijst;
    }

    public function update($idBestelling, $idAccount, $bestelTijd, $leveringsTijd, $mededeling, $idBedrijf)
    {
        $DAO = new BestellingDAO();
        $lijst = $DAO->update($idBestelling, $idAccount, $bestelTijd, $leveringsTijd, $mededeling, $idBedrijf);
        return $lijst;
    }
}