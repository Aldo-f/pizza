<?php
namespace pizza\business\bestelling;

use pizza\data\bestelling\BestellingLijnDAO;

class BestellingLijnService
{
    public function getAll()
    {
        $DAO = new BestellingLijnDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getByIdPromotieLijn($id)
    {
        $DAO = new BestellingLijnDAO();
        $lijst = $DAO->getByIdPromotieLijn($id);
        return $lijst;
    }

    public function getByIdProductLijn($id)
    {
        $DAO = new BestellingLijnDAO();
        $lijst = $DAO->getByIdProductLijn($id);
        return $lijst;
    }

    public function getByIdBestelling($id)
    {
        $DAO = new BestellingLijnDAO();
        $lijst = $DAO->getByIdBestelling($id);
        return $lijst;
    }

    public function create($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn)
    {
        $DAO = new BestellingLijnDAO();
        $lijst = $DAO->create($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn);
        return $lijst;
    }

    public function createBasic($idProductLijn, $idBestelling, $aantalProductLijn)
    {
        $DAO = new BestellingLijnDAO();
        $lijst = $DAO->createBasic($idProductLijn, $idBestelling, $aantalProductLijn);
        return $lijst;
    }


    // nog aan te maken eventueel
    public function updateByIdPromotieLijn($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn)
    {
        $DAO = new BestellingLijnDAO();
        $lijst = $DAO->updateByProductId($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn);
        return $lijst;
    }

    public function updateByIdProductLijn($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn)
    {
        $DAO = new BestellingLijnDAO();
        $lijst = $DAO->updateByBedrijfId($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn);
        return $lijst;
    }

    public function updateByIdBestelling($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn)
    {
        $DAO = new BestellingLijnDAO();
        $lijst = $DAO->updateByBedrijfId($idPromotieLijn, $idProductLijn, $idBestelling, $aantalProductLijn, $aantalPromotieLijn);
        return $lijst;
    }
}
