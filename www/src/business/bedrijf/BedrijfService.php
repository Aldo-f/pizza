<?php
namespace pizza\business\bedrijf;

use pizza\data\bedrijf\BedrijfDAO;


class BedrijfService
{
    public function getAll()
    {
        $DAO = new BedrijfDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getById($id)
    {
        $DAO = new BedrijfDAO();
        $lijst = $DAO->getById($id);
        return $lijst;
    }

    public function create($bedrijfnaam, $adres, $huisnummer, $telefoonnummer, $idPlaats)
    {
        $DAO = new BedrijfDAO();
        $lijst = $DAO->create($bedrijfnaam, $adres, $huisnummer, $telefoonnummer, $idPlaats);
        return $lijst;
    }

    public function update($idBedrijf, $bedrijfnaam, $adres, $huisnummer, $telefoonnummer, $idPlaats)
    {
        $DAO = new BedrijfDAO();
        $lijst = $DAO->update($idBedrijf, $bedrijfnaam, $adres, $huisnummer, $telefoonnummer, $idPlaats);
        return $lijst;
    }
}