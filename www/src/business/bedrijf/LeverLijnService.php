<?php
namespace pizza\business\bedrijf;

use pizza\data\bedrijf\LeverLijnDAO;

class LeverLijnService
{
    public function getAll()
    {
        $DAO = new LeverLijnDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getByIdBedrijf($id)
    {
        $DAO = new LeverLijnDAO();
        $lijst = $DAO->getByIdBedrijf($id);
        return $lijst;
    }

    public function getByIdPlaats($id)
    {
        $DAO = new LeverLijnDAO();
        $lijst = $DAO->getByIdPlaats($id);
        return $lijst;
    }

    public function getByPostcode($id)
    {
        $DAO = new LeverLijnDAO();
        $lijst = $DAO->getByPostcode($id);
        return $lijst;
    }

    public function create($idBedrijf, $idPlaats)
    {
        $DAO = new LeverLijnDAO();
        $lijst = $DAO->create($idBedrijf, $idPlaats);
        return $lijst;
    }

    public function updateByBedrijfId($idBedrijf, $idPlaats)
    {
        $DAO = new LeverLijnDAO();
        $lijst = $DAO->updateByBedrijfId($idBedrijf, $idPlaats);
        return $lijst;
    }

    public function updateByPlaatsId($idBedrijf, $idPlaat)
    {
        $DAO = new LeverLijnDAO();
        $lijst = $DAO->updateByBedrijfId($idBedrijf, $idPlaat);
        return $lijst;
    }
}