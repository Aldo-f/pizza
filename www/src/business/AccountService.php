<?php
namespace pizza\business;

use pizza\data\AccountDAO;

class AccountService
{
    public function getAll()
    {
        $DAO = new AccountDAO();
        $lijst = $DAO->getAll();
        return $lijst;
    }

    public function getById($id)
    {
        $DAO = new AccountDAO();
        $lijst = $DAO->getById($id);
        return $lijst;
    }

    public function getByEmail($email)
    {
        $DAO = new AccountDAO();
        $lijst = $DAO->getByEmail($email);
        return $lijst;
    }

    public function getByEmailWachtwoord($email, $wachwoord)
    {
        $DAO = new AccountDAO();
        $lijst = $DAO->getByEmailWachtwoord($email, $wachwoord);
        return $lijst;
    }

    public function create($naam, $voornaam, $adres, $huisnummer, $telefoonnummer, $email, $wachtwoord, $geboortedatum, $idPlaats)
    {
        $DAO = new AccountDAO();
        $lijst = $DAO->create($naam, $voornaam, $adres, $huisnummer, $telefoonnummer, $email, $wachtwoord, $geboortedatum, $idPlaats);
        return $lijst;
    }

    public function update($idAccount, $naam, $voornaam, $adres, $huisnummer, $telefoonnummer, $email, $wachtwoord, $geboortedatum, $idPlaats)
    {
        $DAO = new AccountDAO();
        $lijst = $DAO->update($idAccount, $naam, $voornaam, $adres, $huisnummer, $telefoonnummer, $email, $wachtwoord, $geboortedatum, $idPlaats);
        return $lijst;
    }
}