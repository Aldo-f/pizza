<?php if (isset($msgSuccess) && (isset($msgSuccess["header"])) && (isset($msgSuccess["msg"]))) {
    ?>
<div class="ui positive message">
    <i class="close icon"></i>
    <div class="header">
        <?php
            echo $msgSuccess["header"] ?>
    </div>
    <p>
        <?php
            if (count($msgSuccess["msg"]) > 1) {
                echo "<ul class='list'>";
                foreach ($msgSuccess["msg"] as $msg) {
                    echo "<li> $msg </li>";
                }
                echo "</ul>";
            } else {
                echo $msgSuccess["msg"][0];
            } ?>
    </p>
</div>
<?php  } ?>

<?php if (isset($msgWarning) && (isset($msgWarning["header"])) && (isset($msgWarning["msg"]))) {
    ?>
<div class="ui warning message">
    <i class="close icon"></i>
    <div class="header">
        <?php
            echo $msgWarning["header"] ?>
    </div>
    <p>
        <?php
            if (count($msgWarning["msg"]) > 1) {
                echo "<ul class='list'>";
                foreach ($msgWarning["msg"] as $msg) {
                    echo "<li> $msg </li>";
                }
                echo "</ul>";
            } else {
                if ($msgWarning["msg"] != null) {
                    echo $msgWarning["msg"][0];
                }
            } ?>
    </p>
</div>

<?php  } ?>

<?php if (isset($msgError) && (isset($msgError["header"])) && (isset($msgError["msg"]))) {
    ?>
<div class="ui error message">
    <i class="close icon"></i>
    <div class="header">
        <?php
            echo $msgError["header"] ?>
    </div>
    <p>
        <?php
            if (count($msgError["msg"]) > 1) {
                echo "<ul class='list'>";
                foreach ($msgError["msg"] as $msg) {
                    echo "<li> $msg </li>";
                }
                echo "</ul>";
            } else {
                if ($msgError["msg"] != null) {
                    echo $msgError["msg"][0];
                }
            } ?>
    </p>
</div>
<?php  } ?>