</div>
<footer class="ui inverted vertical footer segment ">
    <div class="ui center aligned container">
        <!-- <img src="src/assets/img/dflogo.png" class="ui centered mini image"> -->
        <div class="ui horizontal inverted small divided link list">
            made with <i class="heartbeat red icon"></i> by <a target="_blank" href="https://aldo-f.github.io">aldo</a>
        </div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js"></script>

<script src="src/assets/js/script.js"></script>
</body>

</html>