<div class="ui top fixed inverted menu">
    <div class="ui container">
        <a class="header item" href="index.php">
            <img class="ui mini image" src="src/assets/img/logo.png" />
            &nbsp; Pizza delivery
        </a>
        <a class="item" href="index.php">Home</a>
        <!-- <a class="item" href="test.php">Test</a> -->
        <div class="ui simple dropdown item">
            Sub menu<i class="dropdown icon"></i>
            <div class="menu">

                <?php
                if (isset($ingelogd) && $ingelogd == false) {
                    ?>
                <a class="item" href="login.php">Login</a>
                <a class="item" href="registreer.php">Registreer</a>
                <?php
            } else {
                ?>
                <div class="devider"></div>
                <a class="item" href="?actie=logout">Logout</a>
                <a class="item" href="mijnaccount.php">Account</a>
                <div class="ui dropdown item">
                    Meer
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        <a class="item" href="mijnbestellingen.php"><i class="shopping bag icon"></i> Je
                            bestellingen</a>
                    </div>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>