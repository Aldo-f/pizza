CREATE TABLE `pizza`.`bestellinglijn` (
  `idPromotieLijn` INT NOT NULL,
  `idProductLijn` INT NOT NULL,
  `idBestelling` INT NOT NULL,
  `aantalProductLijn` INT NOT NULL COMMENT 'minimaal 1',
  `aantalPromotieLijn` INT NULL COMMENT 'minimaal null of 0\ngeen max',
  PRIMARY KEY (`idPromotieLijn`, `idProductLijn`, `idBestelling`),
  INDEX `fk_BestellingLijn_ProductLijn_idx` (`idProductLijn` ASC),
  INDEX `fk_BestellingLijn_Bestelling_idx` (`idBestelling` ASC),
  CONSTRAINT `fk_BestellingLijn_PromotieLijn`
    FOREIGN KEY (`idPromotieLijn`)
    REFERENCES `pizza`.`promotielijn` (`idPromotieLijn`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_BestellingLijn_ProductLijn`
    FOREIGN KEY (`idProductLijn`)
    REFERENCES `pizza`.`productlijn` (`idProductLijn`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_BestellingLijn_Bestelling`
    FOREIGN KEY (`idBestelling`)
    REFERENCES `pizza`.`bestelling` (`idBestelling`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COMMENT = 'Meerdere lijnen mogelijk per bestelling.\nx aantal keer hetzelfde product (dat beschikbaar is bij het bedrijf)\nx aantal promoties per bestellinglijn (als 1 lijn meerdere keren zelfde pizza bevat, dan kunnen er ook meerdere keren dezelfde korting gegeven worden voor de pizza)';
