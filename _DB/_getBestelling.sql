SELECT bestelling.idBestelling, idAccount, bestelTijd, leveringsTijd, mededeling, prijs, idPromotieLijn, productlijn.idProductLijn, aantalProductLijn, aantalPromotieLijn, idBedrijf, idProduct, beschikbaar 
FROM pizza.bestelling
inner join pizza.bestellinglijn on bestelling.idBestelling = bestellinglijn.idbestelling
inner join pizza.productlijn on bestellinglijn.idproductlijn = productlijn.idproductlijn
-- WHERE bestelling.idBestelling = 2
ORDER BY bestelling.idBestelling