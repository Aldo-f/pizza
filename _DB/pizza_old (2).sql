-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2019 at 09:06 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `idAccount` int(11) NOT NULL,
  `naam` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `voornaam` varchar(45) COLLATE utf8_bin NOT NULL,
  `adres` varchar(45) COLLATE utf8_bin NOT NULL,
  `huisnummer` varchar(45) COLLATE utf8_bin NOT NULL,
  `telefoonnummer` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_bin NOT NULL,
  `wachtwoord` char(128) COLLATE utf8_bin NOT NULL,
  `geboortedatum` date NOT NULL,
  `idPlaats` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`idAccount`, `naam`, `voornaam`, `adres`, `huisnummer`, `telefoonnummer`, `email`, `wachtwoord`, `geboortedatum`, `idPlaats`) VALUES
(4, 'Fieuw', 'Aldo', 'Zilverhof', '18', '0494135450', 'aldo.fieuw@gmail.com', '27c4f77d28b2eb214d206b5497dadb9634996f6e84b598c4bd6230ecbfea35b870236f44b4be42194b28e9c22fedc63916eba263e4c6a1a130327b99d79f8510', '1990-08-16', 1),
(8, 'Familienaam', 'Test', 'Zilhof', '50', '0494123456', 'test@getnada.com', '27c4f77d28b2eb214d206b5497dadb9634996f6e84b598c4bd6230ecbfea35b870236f44b4be42194b28e9c22fedc63916eba263e4c6a1a130327b99d79f8510', '1990-04-01', 8),
(9, 'Fghj', 'Dajele', 'Tvgbh', '55', '04158754', 'dajele@getnada.com', '27c4f77d28b2eb214d206b5497dadb9634996f6e84b598c4bd6230ecbfea35b870236f44b4be42194b28e9c22fedc63916eba263e4c6a1a130327b99d79f8510', '1990-08-16', 8),
(10, 'Admin', 'Pizza', 'Aan De Bocht', '5', '0494123456', 'vynygeko@getnada.com', '35ab157bfe3eddee342463e12c3c1f5013a73da7ee37e93dd1ee2d3e1bbee49805790ca8f6a0c7e9b9e783117189c2b0df9a03dec0088e33d60e914de29429b9', '1990-08-16', 1),
(11, 'Fieuw', 'Aldo', 'Zilverhof', '18', '0494135450', 'aldo@getnada.com', '27c4f77d28b2eb214d206b5497dadb9634996f6e84b598c4bd6230ecbfea35b870236f44b4be42194b28e9c22fedc63916eba263e4c6a1a130327b99d79f8510', '1990-08-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bedrijf`
--

CREATE TABLE `bedrijf` (
  `idBedrijf` int(11) NOT NULL,
  `bedrijfnaam` varchar(45) COLLATE utf8_bin NOT NULL,
  `adres` varchar(45) COLLATE utf8_bin NOT NULL,
  `huisnummer` varchar(45) COLLATE utf8_bin NOT NULL,
  `telefoonnummer` varchar(45) COLLATE utf8_bin NOT NULL,
  `idPlaats` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bedrijf`
--

INSERT INTO `bedrijf` (`idBedrijf`, `bedrijfnaam`, `adres`, `huisnummer`, `telefoonnummer`, `idPlaats`) VALUES
(1, 'Domino\'s Overpoort', 'Overpoortstraat', '57', '092203737', 1),
(2, 'Domino\'s Gent-Sint-Pieters', 'Koningin Astridlaan', '226', '092259000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bedrijflijn`
--

CREATE TABLE `bedrijflijn` (
  `idStatus` int(11) NOT NULL,
  `idBedrijf` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `bestelling`
--

CREATE TABLE `bestelling` (
  `idBestelling` int(11) NOT NULL,
  `idAccount` int(11) NOT NULL,
  `bestelTijd` datetime NOT NULL,
  `leveringsTijd` datetime NOT NULL,
  `mededeling` varchar(240) COLLATE utf8_bin DEFAULT NULL,
  `idBedrijf` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bestelling`
--

INSERT INTO `bestelling` (`idBestelling`, `idAccount`, `bestelTijd`, `leveringsTijd`, `mededeling`, `idBedrijf`) VALUES
(1, 8, '2019-05-07 17:00:30', '2019-05-07 17:50:50', 'Hard klopppen want de bel doet het niet', 2),
(2, 8, '2019-05-08 11:00:00', '2019-05-08 11:40:00', NULL, 2),
(4, 4, '2019-05-08 11:00:00', '2019-05-08 11:50:00', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `bodem`
--

CREATE TABLE `bodem` (
  `idBodem` int(11) NOT NULL,
  `idSamenstelling` int(11) DEFAULT NULL,
  `prijs` decimal(10,0) NOT NULL,
  `naam` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bodem`
--

INSERT INTO `bodem` (`idBodem`, `idSamenstelling`, `prijs`, `naam`) VALUES
(1, 5, '1', 'Classic'),
(2, 6, '2', 'Italian'),
(3, NULL, '3', 'Cheesy Crust');

-- --------------------------------------------------------

--
-- Table structure for table `formaat`
--

CREATE TABLE `formaat` (
  `idFormaat` int(11) NOT NULL,
  `idSamenstelling` int(11) DEFAULT NULL,
  `naam` varchar(45) COLLATE utf8_bin NOT NULL,
  `prijs` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `formaat`
--

INSERT INTO `formaat` (`idFormaat`, `idSamenstelling`, `naam`, `prijs`) VALUES
(1, NULL, 'Regular', '1'),
(2, NULL, 'Medium', '2'),
(3, NULL, 'Large', '3'),
(4, NULL, 'XXL', '4');

-- --------------------------------------------------------

--
-- Table structure for table `ingredient`
--

CREATE TABLE `ingredient` (
  `idIngredient` int(11) NOT NULL,
  `idSamenstelling` int(11) DEFAULT NULL,
  `naam` varchar(45) COLLATE utf8_bin NOT NULL,
  `prijs` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ingredient`
--

INSERT INTO `ingredient` (`idIngredient`, `idSamenstelling`, `naam`, `prijs`) VALUES
(1, NULL, 'ananas', '1'),
(2, NULL, 'augurk', '1'),
(3, NULL, 'bacon', '1'),
(4, NULL, 'champignons', '1'),
(5, NULL, 'emmentaler', '1'),
(6, NULL, 'garnalen', '1'),
(7, NULL, 'gehakt', '1'),
(8, NULL, 'geitenkaas', '1'),
(9, NULL, 'gorgonzola', '1'),
(10, NULL, 'grilled veggie mix', '1'),
(11, NULL, 'ham', '1'),
(12, NULL, 'honing', '1'),
(13, NULL, 'kip kebap halal', '1'),
(14, NULL, 'kip kebap', '1'),
(15, NULL, 'lente-uitjes', '1'),
(16, NULL, 'mediterrane provence kruiden', '1'),
(17, NULL, 'merguez', '1'),
(18, NULL, 'mozzarella', '1'),
(19, NULL, 'paprika', '1'),
(20, NULL, 'pepperoni', '1'),
(21, NULL, 'reblochon', '1'),
(22, NULL, 'rode ui', '1'),
(23, NULL, 'ruindvleesstrips', '1'),
(24, NULL, 'spaanse pepers', '1'),
(25, NULL, 'spinazie', '1'),
(26, NULL, 'tonijn', '1'),
(27, NULL, 'vegan kaas', '1'),
(28, NULL, 'tomaat', '1'),
(29, NULL, 'zwarte olijven', '1'),
(30, NULL, 'aardappelschijfjes', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ingridientlijn`
--

CREATE TABLE `ingridientlijn` (
  `idIngredient` int(11) NOT NULL,
  `idProduct` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ingridientlijn`
--

INSERT INTO `ingridientlijn` (`idIngredient`, `idProduct`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(3, 2),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(7, 3),
(8, 1),
(17, 3),
(18, 2),
(18, 3),
(21, 2),
(22, 3),
(24, 3),
(30, 2);

-- --------------------------------------------------------

--
-- Table structure for table `leverlijn`
--

CREATE TABLE `leverlijn` (
  `idBedrijf` int(11) NOT NULL,
  `idPlaats` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `leverlijn`
--

INSERT INTO `leverlijn` (`idBedrijf`, `idPlaats`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 8),
(2, 1),
(2, 2),
(2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `openingsdag`
--

CREATE TABLE `openingsdag` (
  `idOpeningsdag` int(11) NOT NULL,
  `naam` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'maandag = 1\nzondag = 7\nfeestdag = 8'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `openingsdaglijn`
--

CREATE TABLE `openingsdaglijn` (
  `idBedrijf` int(11) NOT NULL,
  `idOpeningsdag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `openingsuur`
--

CREATE TABLE `openingsuur` (
  `idOpeningsuur` int(11) NOT NULL,
  `van` time NOT NULL,
  `tot` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `openingsuurlijn`
--

CREATE TABLE `openingsuurlijn` (
  `idOpeningsuur` int(11) NOT NULL,
  `idOpeningsdag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `plaats`
--

CREATE TABLE `plaats` (
  `idPlaats` int(11) NOT NULL,
  `postcode` varchar(10) COLLATE utf8_bin NOT NULL,
  `plaats` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `plaats`
--

INSERT INTO `plaats` (`idPlaats`, `postcode`, `plaats`) VALUES
(1, '9000', 'Gent'),
(2, '9050', 'Gentbrugge'),
(3, '9050', 'Ledeberg'),
(8, '1000', 'Brussel'),
(9, '1000', 'Nieuwe Gemeente , Bestaande Postcode');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `idProduct` int(11) NOT NULL,
  `naam` varchar(20) COLLATE utf8_bin NOT NULL,
  `omschrijving` varchar(140) COLLATE utf8_bin NOT NULL,
  `idFormaat` int(11) NOT NULL,
  `idBodem` int(11) NOT NULL,
  `idSaus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`idProduct`, `naam`, `omschrijving`, `idFormaat`, `idBodem`, `idSaus`) VALUES
(1, 'pizza random', 'pizza samengesteld uit random ingredienten', 1, 1, 1),
(2, 'savoyarde', 'wil je zeker genoeg hebben? kies dan deze pizza', 3, 3, 2),
(3, 'pizza diabolika', 'voor wie het wat pikant wilt', 4, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `productlijn`
--

CREATE TABLE `productlijn` (
  `idBedrijf` int(11) NOT NULL,
  `idProduct` int(11) NOT NULL,
  `beschikbaar` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `productlijn`
--

INSERT INTO `productlijn` (`idBedrijf`, `idProduct`, `beschikbaar`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 1),
(2, 1, 1),
(2, 2, 1),
(2, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `promotielijn`
--

CREATE TABLE `promotielijn` (
  `idBedrijf` int(11) NOT NULL,
  `idPromoties` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `promoties`
--

CREATE TABLE `promoties` (
  `idPromoties` int(11) NOT NULL,
  `naam` varchar(45) COLLATE utf8_bin NOT NULL,
  `omschrijving` varchar(140) COLLATE utf8_bin DEFAULT NULL,
  `code` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `idProduct` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `samenstelling`
--

CREATE TABLE `samenstelling` (
  `idSamenstelling` int(11) NOT NULL,
  `naam` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'Vlees, vis, glutten,vegie, kip',
  `calorieën` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `categorie` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `samenstelling`
--

INSERT INTO `samenstelling` (`idSamenstelling`, `naam`, `calorieën`, `categorie`) VALUES
(1, 'vlees', NULL, 'ingredient'),
(2, 'kaas', NULL, 'bodem'),
(3, 'kaas', NULL, 'ingredient'),
(4, 'vis', NULL, 'ingredient'),
(5, 'gluten', NULL, 'bodem'),
(6, 'glutenvrij', NULL, 'bodem'),
(7, 'kaas', NULL, 'saus');

-- --------------------------------------------------------

--
-- Table structure for table `saus`
--

CREATE TABLE `saus` (
  `idSaus` int(11) NOT NULL,
  `idSamenstelling` int(11) DEFAULT NULL,
  `naam` varchar(45) COLLATE utf8_bin NOT NULL,
  `prijs` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `saus`
--

INSERT INTO `saus` (`idSaus`, `idSamenstelling`, `naam`, `prijs`) VALUES
(1, NULL, 'BBQ-saus', '2'),
(2, 7, 'Crème Fraîche', '2'),
(3, NULL, 'Tomatensaus', '1');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `idStatus` int(11) NOT NULL,
  `naam` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'Als de status admin is, dan kan er verder gecontroleerd worden aan welk bedrijf, of bedrijven ingelogd persoon gekoppeld is',
  `omschrijving` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `statuslijn`
--

CREATE TABLE `statuslijn` (
  `idAccount` int(11) NOT NULL,
  `idStatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`idAccount`),
  ADD KEY `fk_Account_Plaats1_idx` (`idPlaats`);

--
-- Indexes for table `bedrijf`
--
ALTER TABLE `bedrijf`
  ADD PRIMARY KEY (`idBedrijf`),
  ADD KEY `fk_Bedrijf_Plaats1_idx` (`idPlaats`);

--
-- Indexes for table `bedrijflijn`
--
ALTER TABLE `bedrijflijn`
  ADD PRIMARY KEY (`idStatus`,`idBedrijf`),
  ADD KEY `fk_BedrijfLijn_Bedrijf1_idx` (`idBedrijf`);

--
-- Indexes for table `bestelling`
--
ALTER TABLE `bestelling`
  ADD PRIMARY KEY (`idBestelling`),
  ADD KEY `fk_Bestelling_Account1_idx` (`idAccount`),
  ADD KEY `fk_Bestelling_Bedrijf1_idx` (`idBedrijf`);

--
-- Indexes for table `bodem`
--
ALTER TABLE `bodem`
  ADD PRIMARY KEY (`idBodem`),
  ADD KEY `fk_Bodem_Samenstelling1_idx` (`idSamenstelling`);

--
-- Indexes for table `formaat`
--
ALTER TABLE `formaat`
  ADD PRIMARY KEY (`idFormaat`),
  ADD KEY `fk_Formaat_Samenstelling1_idx` (`idSamenstelling`);

--
-- Indexes for table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`idIngredient`),
  ADD KEY `fk_Ingredient_Samenstelling1_idx` (`idSamenstelling`);

--
-- Indexes for table `ingridientlijn`
--
ALTER TABLE `ingridientlijn`
  ADD PRIMARY KEY (`idIngredient`,`idProduct`),
  ADD KEY `fk_Ingredient_has_Product_Product1_idx` (`idProduct`),
  ADD KEY `fk_Ingredient_has_Product_Ingredient1_idx` (`idIngredient`);

--
-- Indexes for table `leverlijn`
--
ALTER TABLE `leverlijn`
  ADD PRIMARY KEY (`idBedrijf`,`idPlaats`),
  ADD KEY `fk_LeverLijn_Plaats1_idx` (`idPlaats`);

--
-- Indexes for table `openingsdag`
--
ALTER TABLE `openingsdag`
  ADD PRIMARY KEY (`idOpeningsdag`),
  ADD UNIQUE KEY `naam_UNIQUE` (`naam`);

--
-- Indexes for table `openingsdaglijn`
--
ALTER TABLE `openingsdaglijn`
  ADD PRIMARY KEY (`idBedrijf`,`idOpeningsdag`),
  ADD KEY `fk_Bedrijf_has_Openingsdag_Openingsdag1_idx` (`idOpeningsdag`),
  ADD KEY `fk_Bedrijf_has_Openingsdag_Bedrijf1_idx` (`idBedrijf`);

--
-- Indexes for table `openingsuur`
--
ALTER TABLE `openingsuur`
  ADD PRIMARY KEY (`idOpeningsuur`);

--
-- Indexes for table `openingsuurlijn`
--
ALTER TABLE `openingsuurlijn`
  ADD PRIMARY KEY (`idOpeningsuur`,`idOpeningsdag`),
  ADD KEY `fk_Openingsuur_has_Openingsdag_Openingsdag1_idx` (`idOpeningsdag`),
  ADD KEY `fk_Openingsuur_has_Openingsdag_Openingsuur1_idx` (`idOpeningsuur`);

--
-- Indexes for table `plaats`
--
ALTER TABLE `plaats`
  ADD PRIMARY KEY (`idPlaats`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`idProduct`),
  ADD KEY `fk_Product_Formaat1_idx` (`idFormaat`),
  ADD KEY `fk_Product_Bodem1_idx` (`idBodem`),
  ADD KEY `fk_Product_Saus1_idx` (`idSaus`);

--
-- Indexes for table `productlijn`
--
ALTER TABLE `productlijn`
  ADD PRIMARY KEY (`idBedrijf`,`idProduct`),
  ADD KEY `fk_Bedrijf_has_Product_Product1_idx` (`idProduct`),
  ADD KEY `fk_Bedrijf_has_Product_Bedrijf1_idx` (`idBedrijf`);

--
-- Indexes for table `promotielijn`
--
ALTER TABLE `promotielijn`
  ADD PRIMARY KEY (`idBedrijf`,`idPromoties`),
  ADD KEY `fk_Bedrijf_has_Promoties_Promoties1_idx` (`idPromoties`),
  ADD KEY `fk_Bedrijf_has_Promoties_Bedrijf1_idx` (`idBedrijf`);

--
-- Indexes for table `promoties`
--
ALTER TABLE `promoties`
  ADD PRIMARY KEY (`idPromoties`),
  ADD KEY `fk_Promoties_Product1_idx` (`idProduct`);

--
-- Indexes for table `samenstelling`
--
ALTER TABLE `samenstelling`
  ADD PRIMARY KEY (`idSamenstelling`);

--
-- Indexes for table `saus`
--
ALTER TABLE `saus`
  ADD PRIMARY KEY (`idSaus`),
  ADD KEY `fk_Saus_Samenstelling1_idx` (`idSamenstelling`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`idStatus`);

--
-- Indexes for table `statuslijn`
--
ALTER TABLE `statuslijn`
  ADD PRIMARY KEY (`idAccount`,`idStatus`),
  ADD KEY `fk_StatusLijn_Status1_idx` (`idStatus`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `idAccount` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bedrijf`
--
ALTER TABLE `bedrijf`
  MODIFY `idBedrijf` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bestelling`
--
ALTER TABLE `bestelling`
  MODIFY `idBestelling` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bodem`
--
ALTER TABLE `bodem`
  MODIFY `idBodem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `formaat`
--
ALTER TABLE `formaat`
  MODIFY `idFormaat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `idIngredient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `openingsuur`
--
ALTER TABLE `openingsuur`
  MODIFY `idOpeningsuur` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plaats`
--
ALTER TABLE `plaats`
  MODIFY `idPlaats` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `idProduct` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `samenstelling`
--
ALTER TABLE `samenstelling`
  MODIFY `idSamenstelling` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `saus`
--
ALTER TABLE `saus`
  MODIFY `idSaus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `idStatus` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `fk_Account_Plaats1` FOREIGN KEY (`idPlaats`) REFERENCES `plaats` (`idPlaats`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bedrijf`
--
ALTER TABLE `bedrijf`
  ADD CONSTRAINT `fk_Bedrijf_Plaats1` FOREIGN KEY (`idPlaats`) REFERENCES `plaats` (`idPlaats`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bedrijflijn`
--
ALTER TABLE `bedrijflijn`
  ADD CONSTRAINT `fk_BedrijfLijn_Bedrijf1` FOREIGN KEY (`idBedrijf`) REFERENCES `bedrijf` (`idBedrijf`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_BedrijfLijn_Status1` FOREIGN KEY (`idStatus`) REFERENCES `status` (`idStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bestelling`
--
ALTER TABLE `bestelling`
  ADD CONSTRAINT `fk_Bestelling_Account1` FOREIGN KEY (`idAccount`) REFERENCES `account` (`idAccount`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Bestelling_Bedrijf1` FOREIGN KEY (`idBedrijf`) REFERENCES `bedrijf` (`idBedrijf`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bodem`
--
ALTER TABLE `bodem`
  ADD CONSTRAINT `fk_Bodem_Samenstelling1` FOREIGN KEY (`idSamenstelling`) REFERENCES `samenstelling` (`idSamenstelling`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `formaat`
--
ALTER TABLE `formaat`
  ADD CONSTRAINT `fk_Formaat_Samenstelling1` FOREIGN KEY (`idSamenstelling`) REFERENCES `samenstelling` (`idSamenstelling`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ingredient`
--
ALTER TABLE `ingredient`
  ADD CONSTRAINT `fk_Ingredient_Samenstelling1` FOREIGN KEY (`idSamenstelling`) REFERENCES `samenstelling` (`idSamenstelling`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ingridientlijn`
--
ALTER TABLE `ingridientlijn`
  ADD CONSTRAINT `fk_Ingredient_has_Product_Ingredient1` FOREIGN KEY (`idIngredient`) REFERENCES `ingredient` (`idIngredient`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Ingredient_has_Product_Product1` FOREIGN KEY (`idProduct`) REFERENCES `product` (`idProduct`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `leverlijn`
--
ALTER TABLE `leverlijn`
  ADD CONSTRAINT `fk_LeverLijn_Bedrijf1` FOREIGN KEY (`idBedrijf`) REFERENCES `bedrijf` (`idBedrijf`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_LeverLijn_Plaats1` FOREIGN KEY (`idPlaats`) REFERENCES `plaats` (`idPlaats`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `openingsdaglijn`
--
ALTER TABLE `openingsdaglijn`
  ADD CONSTRAINT `fk_Bedrijf_has_Openingsdag_Bedrijf1` FOREIGN KEY (`idBedrijf`) REFERENCES `bedrijf` (`idBedrijf`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Bedrijf_has_Openingsdag_Openingsdag1` FOREIGN KEY (`idOpeningsdag`) REFERENCES `openingsdag` (`idOpeningsdag`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `openingsuurlijn`
--
ALTER TABLE `openingsuurlijn`
  ADD CONSTRAINT `fk_Openingsuur_has_Openingsdag_Openingsdag1` FOREIGN KEY (`idOpeningsdag`) REFERENCES `openingsdag` (`idOpeningsdag`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Openingsuur_has_Openingsdag_Openingsuur1` FOREIGN KEY (`idOpeningsuur`) REFERENCES `openingsuur` (`idOpeningsuur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_Product_Bodem1` FOREIGN KEY (`idBodem`) REFERENCES `bodem` (`idBodem`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Product_Formaat1` FOREIGN KEY (`idFormaat`) REFERENCES `formaat` (`idFormaat`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Product_Saus1` FOREIGN KEY (`idSaus`) REFERENCES `saus` (`idSaus`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `productlijn`
--
ALTER TABLE `productlijn`
  ADD CONSTRAINT `fk_Bedrijf_has_Product_Bedrijf1` FOREIGN KEY (`idBedrijf`) REFERENCES `bedrijf` (`idBedrijf`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Bedrijf_has_Product_Product1` FOREIGN KEY (`idProduct`) REFERENCES `product` (`idProduct`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `promotielijn`
--
ALTER TABLE `promotielijn`
  ADD CONSTRAINT `fk_Bedrijf_has_Promoties_Bedrijf1` FOREIGN KEY (`idBedrijf`) REFERENCES `bedrijf` (`idBedrijf`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Bedrijf_has_Promoties_Promoties1` FOREIGN KEY (`idPromoties`) REFERENCES `promoties` (`idPromoties`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `promoties`
--
ALTER TABLE `promoties`
  ADD CONSTRAINT `fk_Promoties_Product1` FOREIGN KEY (`idProduct`) REFERENCES `product` (`idProduct`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `saus`
--
ALTER TABLE `saus`
  ADD CONSTRAINT `fk_Saus_Samenstelling1` FOREIGN KEY (`idSamenstelling`) REFERENCES `samenstelling` (`idSamenstelling`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `statuslijn`
--
ALTER TABLE `statuslijn`
  ADD CONSTRAINT `fk_StatusLijn_Account1` FOREIGN KEY (`idAccount`) REFERENCES `account` (`idAccount`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_StatusLijn_Status1` FOREIGN KEY (`idStatus`) REFERENCES `status` (`idStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
