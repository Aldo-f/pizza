-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema pizza
-- -----------------------------------------------------
-- Pizza schema voor eindoefening PHP

-- -----------------------------------------------------
-- Schema pizza
--
-- Pizza schema voor eindoefening PHP
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `pizza` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `pizza` ;

-- -----------------------------------------------------
-- Table `pizza`.`Plaats`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Plaats` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Plaats` (
  `idPlaats` INT NOT NULL AUTO_INCREMENT,
  `postcode` VARCHAR(10) NOT NULL,
  `plaats` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPlaats`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Account` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Account` (
  `idAccount` INT NOT NULL AUTO_INCREMENT,
  `naam` VARCHAR(45) NULL,
  `voornaam` VARCHAR(45) NOT NULL,
  `adres` VARCHAR(45) NOT NULL,
  `huisnummer` VARCHAR(45) NOT NULL,
  `telefoonnummer` VARCHAR(45) NULL,
  `email` VARCHAR(45) NOT NULL,
  `wachtwoord` CHAR(128) NOT NULL,
  `geboortedatum` DATE NOT NULL,
  `idPlaats` INT NOT NULL,
  PRIMARY KEY (`idAccount`),
  INDEX `fk_Account_Plaats1_idx` (`idPlaats` ASC) VISIBLE,
  CONSTRAINT `fk_Account_Plaats1`
    FOREIGN KEY (`idPlaats`)
    REFERENCES `pizza`.`Plaats` (`idPlaats`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Status` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Status` (
  `idStatus` INT NOT NULL AUTO_INCREMENT,
  `naam` VARCHAR(10) NOT NULL COMMENT 'Als de status admin is, dan kan er verder gecontroleerd worden aan welk bedrijf, of bedrijven ingelogd persoon gekoppeld is',
  `omschrijving` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idStatus`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`StatusLijn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`StatusLijn` ;

CREATE TABLE IF NOT EXISTS `pizza`.`StatusLijn` (
  `idAccount` INT NOT NULL,
  `idStatus` INT NOT NULL,
  PRIMARY KEY (`idAccount`, `idStatus`),
  INDEX `fk_StatusLijn_Status1_idx` (`idStatus` ASC) VISIBLE,
  CONSTRAINT `fk_StatusLijn_Account1`
    FOREIGN KEY (`idAccount`)
    REFERENCES `pizza`.`Account` (`idAccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_StatusLijn_Status1`
    FOREIGN KEY (`idStatus`)
    REFERENCES `pizza`.`Status` (`idStatus`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Bedrijf`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Bedrijf` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Bedrijf` (
  `idBedrijf` INT NOT NULL AUTO_INCREMENT,
  `bedrijfnaam` VARCHAR(45) NOT NULL,
  `adres` VARCHAR(45) NOT NULL,
  `huisnummer` VARCHAR(45) NOT NULL,
  `telefoonnummer` VARCHAR(45) NOT NULL,
  `idPlaats` INT NOT NULL,
  PRIMARY KEY (`idBedrijf`),
  INDEX `fk_Bedrijf_Plaats1_idx` (`idPlaats` ASC) VISIBLE,
  CONSTRAINT `fk_Bedrijf_Plaats1`
    FOREIGN KEY (`idPlaats`)
    REFERENCES `pizza`.`Plaats` (`idPlaats`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`LeverLijn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`LeverLijn` ;

CREATE TABLE IF NOT EXISTS `pizza`.`LeverLijn` (
  `idBedrijf` INT NOT NULL,
  `idPlaats` INT NOT NULL,
  PRIMARY KEY (`idBedrijf`, `idPlaats`),
  INDEX `fk_LeverLijn_Plaats1_idx` (`idPlaats` ASC) VISIBLE,
  CONSTRAINT `fk_LeverLijn_Bedrijf1`
    FOREIGN KEY (`idBedrijf`)
    REFERENCES `pizza`.`Bedrijf` (`idBedrijf`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_LeverLijn_Plaats1`
    FOREIGN KEY (`idPlaats`)
    REFERENCES `pizza`.`Plaats` (`idPlaats`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`BedrijfLijn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`BedrijfLijn` ;

CREATE TABLE IF NOT EXISTS `pizza`.`BedrijfLijn` (
  `idStatus` INT NOT NULL,
  `idBedrijf` INT NOT NULL,
  PRIMARY KEY (`idStatus`, `idBedrijf`),
  INDEX `fk_BedrijfLijn_Bedrijf1_idx` (`idBedrijf` ASC) VISIBLE,
  CONSTRAINT `fk_BedrijfLijn_Status1`
    FOREIGN KEY (`idStatus`)
    REFERENCES `pizza`.`Status` (`idStatus`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_BedrijfLijn_Bedrijf1`
    FOREIGN KEY (`idBedrijf`)
    REFERENCES `pizza`.`Bedrijf` (`idBedrijf`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Samenstelling`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Samenstelling` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Samenstelling` (
  `idSamenstelling` INT NOT NULL AUTO_INCREMENT,
  `naam` VARCHAR(45) NOT NULL COMMENT 'Vlees, vis, glutten,vegie, kip',
  `calorieën` VARCHAR(45) NULL,
  PRIMARY KEY (`idSamenstelling`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Formaat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Formaat` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Formaat` (
  `idFormaat` INT NOT NULL AUTO_INCREMENT,
  `idSamenstelling` INT NULL,
  `naam` VARCHAR(45) NOT NULL,
  `prijs` DECIMAL NOT NULL,
  PRIMARY KEY (`idFormaat`),
  INDEX `fk_Formaat_Samenstelling1_idx` (`idSamenstelling` ASC) VISIBLE,
  CONSTRAINT `fk_Formaat_Samenstelling1`
    FOREIGN KEY (`idSamenstelling`)
    REFERENCES `pizza`.`Samenstelling` (`idSamenstelling`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Bodem`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Bodem` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Bodem` (
  `idBodem` INT NOT NULL AUTO_INCREMENT,
  `idSamenstelling` INT NULL,
  `prijs` DECIMAL NOT NULL,
  `naam` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idBodem`),
  INDEX `fk_Bodem_Samenstelling1_idx` (`idSamenstelling` ASC) VISIBLE,
  CONSTRAINT `fk_Bodem_Samenstelling1`
    FOREIGN KEY (`idSamenstelling`)
    REFERENCES `pizza`.`Samenstelling` (`idSamenstelling`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Saus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Saus` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Saus` (
  `idSaus` INT NOT NULL AUTO_INCREMENT,
  `idSamenstelling` INT NULL,
  `naam` VARCHAR(45) NOT NULL,
  `prijs` DECIMAL NOT NULL,
  PRIMARY KEY (`idSaus`),
  INDEX `fk_Saus_Samenstelling1_idx` (`idSamenstelling` ASC) VISIBLE,
  CONSTRAINT `fk_Saus_Samenstelling1`
    FOREIGN KEY (`idSamenstelling`)
    REFERENCES `pizza`.`Samenstelling` (`idSamenstelling`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Product` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Product` (
  `idProduct` INT NOT NULL AUTO_INCREMENT,
  `naam` VARCHAR(20) NOT NULL,
  `omschrijving` VARCHAR(140) NOT NULL,
  `idFormaat` INT NOT NULL,
  `idBodem` INT NOT NULL,
  `idSaus` INT NOT NULL,
  PRIMARY KEY (`idProduct`),
  INDEX `fk_Product_Formaat1_idx` (`idFormaat` ASC) VISIBLE,
  INDEX `fk_Product_Bodem1_idx` (`idBodem` ASC) VISIBLE,
  INDEX `fk_Product_Saus1_idx` (`idSaus` ASC) VISIBLE,
  CONSTRAINT `fk_Product_Formaat1`
    FOREIGN KEY (`idFormaat`)
    REFERENCES `pizza`.`Formaat` (`idFormaat`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_Bodem1`
    FOREIGN KEY (`idBodem`)
    REFERENCES `pizza`.`Bodem` (`idBodem`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_Saus1`
    FOREIGN KEY (`idSaus`)
    REFERENCES `pizza`.`Saus` (`idSaus`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Ingredient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Ingredient` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Ingredient` (
  `idIngredient` INT NOT NULL AUTO_INCREMENT,
  `idSamenstelling` INT NULL,
  `naam` VARCHAR(45) NOT NULL,
  `prijs` DECIMAL NOT NULL,
  PRIMARY KEY (`idIngredient`),
  INDEX `fk_Ingredient_Samenstelling1_idx` (`idSamenstelling` ASC) VISIBLE,
  CONSTRAINT `fk_Ingredient_Samenstelling1`
    FOREIGN KEY (`idSamenstelling`)
    REFERENCES `pizza`.`Samenstelling` (`idSamenstelling`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`ProductLijn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`ProductLijn` ;

CREATE TABLE IF NOT EXISTS `pizza`.`ProductLijn` (
  `idBedrijf` INT NOT NULL,
  `idProduct` INT NOT NULL,
  `beschikbaar` TINYBLOB NOT NULL,
  PRIMARY KEY (`idBedrijf`, `idProduct`),
  INDEX `fk_Bedrijf_has_Product_Product1_idx` (`idProduct` ASC) VISIBLE,
  INDEX `fk_Bedrijf_has_Product_Bedrijf1_idx` (`idBedrijf` ASC) VISIBLE,
  CONSTRAINT `fk_Bedrijf_has_Product_Bedrijf1`
    FOREIGN KEY (`idBedrijf`)
    REFERENCES `pizza`.`Bedrijf` (`idBedrijf`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bedrijf_has_Product_Product1`
    FOREIGN KEY (`idProduct`)
    REFERENCES `pizza`.`Product` (`idProduct`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`IngridientLijn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`IngridientLijn` ;

CREATE TABLE IF NOT EXISTS `pizza`.`IngridientLijn` (
  `idIngredient` INT NOT NULL,
  `idProduct` INT NOT NULL,
  PRIMARY KEY (`idIngredient`, `idProduct`),
  INDEX `fk_Ingredient_has_Product_Product1_idx` (`idProduct` ASC) VISIBLE,
  INDEX `fk_Ingredient_has_Product_Ingredient1_idx` (`idIngredient` ASC) VISIBLE,
  CONSTRAINT `fk_Ingredient_has_Product_Ingredient1`
    FOREIGN KEY (`idIngredient`)
    REFERENCES `pizza`.`Ingredient` (`idIngredient`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ingredient_has_Product_Product1`
    FOREIGN KEY (`idProduct`)
    REFERENCES `pizza`.`Product` (`idProduct`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Bestelling`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Bestelling` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Bestelling` (
  `idBestelling` INT NOT NULL,
  `idAccount` INT NOT NULL,
  `bestelTijd` DATETIME NOT NULL,
  `leveringsTijd` DATETIME NOT NULL,
  `mededeling` VARCHAR(240) NULL,
  `idBedrijf` INT NOT NULL,
  PRIMARY KEY (`idBestelling`),
  INDEX `fk_Bestelling_Account1_idx` (`idAccount` ASC) VISIBLE,
  INDEX `fk_Bestelling_Bedrijf1_idx` (`idBedrijf` ASC) VISIBLE,
  CONSTRAINT `fk_Bestelling_Account1`
    FOREIGN KEY (`idAccount`)
    REFERENCES `pizza`.`Account` (`idAccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bestelling_Bedrijf1`
    FOREIGN KEY (`idBedrijf`)
    REFERENCES `pizza`.`Bedrijf` (`idBedrijf`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Openingsdag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Openingsdag` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Openingsdag` (
  `idOpeningsdag` INT NOT NULL,
  `naam` VARCHAR(45) NOT NULL COMMENT 'maandag = 1\nzondag = 7\nfeestdag = 8',
  PRIMARY KEY (`idOpeningsdag`),
  UNIQUE INDEX `naam_UNIQUE` (`naam` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Openingsuur`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Openingsuur` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Openingsuur` (
  `idOpeningsuur` INT NOT NULL AUTO_INCREMENT,
  `van` TIME NOT NULL,
  `tot` TIME NOT NULL,
  PRIMARY KEY (`idOpeningsuur`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`OpeningsuurLijn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`OpeningsuurLijn` ;

CREATE TABLE IF NOT EXISTS `pizza`.`OpeningsuurLijn` (
  `idOpeningsuur` INT NOT NULL,
  `idOpeningsdag` INT NOT NULL,
  PRIMARY KEY (`idOpeningsuur`, `idOpeningsdag`),
  INDEX `fk_Openingsuur_has_Openingsdag_Openingsdag1_idx` (`idOpeningsdag` ASC) VISIBLE,
  INDEX `fk_Openingsuur_has_Openingsdag_Openingsuur1_idx` (`idOpeningsuur` ASC) VISIBLE,
  CONSTRAINT `fk_Openingsuur_has_Openingsdag_Openingsuur1`
    FOREIGN KEY (`idOpeningsuur`)
    REFERENCES `pizza`.`Openingsuur` (`idOpeningsuur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Openingsuur_has_Openingsdag_Openingsdag1`
    FOREIGN KEY (`idOpeningsdag`)
    REFERENCES `pizza`.`Openingsdag` (`idOpeningsdag`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`OpeningsdagLijn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`OpeningsdagLijn` ;

CREATE TABLE IF NOT EXISTS `pizza`.`OpeningsdagLijn` (
  `idBedrijf` INT NOT NULL,
  `idOpeningsdag` INT NOT NULL,
  PRIMARY KEY (`idBedrijf`, `idOpeningsdag`),
  INDEX `fk_Bedrijf_has_Openingsdag_Openingsdag1_idx` (`idOpeningsdag` ASC) VISIBLE,
  INDEX `fk_Bedrijf_has_Openingsdag_Bedrijf1_idx` (`idBedrijf` ASC) VISIBLE,
  CONSTRAINT `fk_Bedrijf_has_Openingsdag_Bedrijf1`
    FOREIGN KEY (`idBedrijf`)
    REFERENCES `pizza`.`Bedrijf` (`idBedrijf`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bedrijf_has_Openingsdag_Openingsdag1`
    FOREIGN KEY (`idOpeningsdag`)
    REFERENCES `pizza`.`Openingsdag` (`idOpeningsdag`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`Promoties`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`Promoties` ;

CREATE TABLE IF NOT EXISTS `pizza`.`Promoties` (
  `idPromoties` INT NOT NULL,
  `naam` VARCHAR(45) NOT NULL,
  `omschrijving` VARCHAR(140) NULL,
  `code` VARCHAR(45) NULL,
  `idProduct` INT NULL,
  PRIMARY KEY (`idPromoties`),
  INDEX `fk_Promoties_Product1_idx` (`idProduct` ASC) VISIBLE,
  CONSTRAINT `fk_Promoties_Product1`
    FOREIGN KEY (`idProduct`)
    REFERENCES `pizza`.`Product` (`idProduct`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza`.`PromotieLijn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizza`.`PromotieLijn` ;

CREATE TABLE IF NOT EXISTS `pizza`.`PromotieLijn` (
  `idBedrijf` INT NOT NULL,
  `idPromoties` INT NOT NULL,
  PRIMARY KEY (`idBedrijf`, `idPromoties`),
  INDEX `fk_Bedrijf_has_Promoties_Promoties1_idx` (`idPromoties` ASC) VISIBLE,
  INDEX `fk_Bedrijf_has_Promoties_Bedrijf1_idx` (`idBedrijf` ASC) VISIBLE,
  CONSTRAINT `fk_Bedrijf_has_Promoties_Bedrijf1`
    FOREIGN KEY (`idBedrijf`)
    REFERENCES `pizza`.`Bedrijf` (`idBedrijf`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bedrijf_has_Promoties_Promoties1`
    FOREIGN KEY (`idPromoties`)
    REFERENCES `pizza`.`Promoties` (`idPromoties`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `pizza`.`Plaats`
-- -----------------------------------------------------
START TRANSACTION;
USE `pizza`;
INSERT INTO `pizza`.`Plaats` (`idPlaats`, `postcode`, `plaats`) VALUES (DEFAULT, '9000', 'Gent');

COMMIT;


-- -----------------------------------------------------
-- Data for table `pizza`.`Status`
-- -----------------------------------------------------
START TRANSACTION;
USE `pizza`;
INSERT INTO `pizza`.`Status` (`idStatus`, `naam`, `omschrijving`) VALUES (DEFAULT, 'admin', 'Kan aanpassingen doen aan bedrijf');
INSERT INTO `pizza`.`Status` (`idStatus`, `naam`, `omschrijving`) VALUES (DEFAULT, 'klant', 'Kan aankopen doen');

COMMIT;

