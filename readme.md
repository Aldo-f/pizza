# Pizza

### Overzicht
* Niet geregistreerd -> bekijken
* Winkelmandje (steeds te zien, sidebar?)
* Mogelijkheid tot toevoegen/verwijderen
* --> Afrekenen knop
  * Niet aangemeld 
    * ``Login``
    * ``Registreer``
  * Ga naar ``Afrekenen``
* eventueel
  * Ingelog -> iniden reeds besteld -> herhaal-laatste-bestelling knop (zonder eventuele promoties)

### Login
* e-mail + wachtwoord
* na login -> overzicht van afrekenen (indien van ``Overzicht`` komt?)

### Registreer
* naam, voornaam, adres, postcode, gemeente, telefoonnummer
* selctieknop: "creëer account"
  * emailadres + wachtwoord (automatisch tonen wnr aangevinkt (JS))
* --> Doorsturen naar ``Afrekenen``

### Afrekenen
* Overzicht van de bestelling:
  * ``pizzaLijst.twig`` (met totaalprijs)
  * Melding wnr postcode is bij plaats waar niet geleverd kan worden
  * -> Adresgegevens aanpassen (``Account``)
  * mogelijkheid om winkelmand bij te werken (als link (of op overzicht van ``Afrekenen`` zelf)
  * eventueel: 
    * extra veld voor opmerkingen te plaatsen<br> 
    (zoals woonst zit verscholen achter het riet; je moet kloppen want de bel doet het niet)
    * pizza aanpassen:  
      * Formaat
      * zonder champignons, met salami
    * Levertijd:
      * 20:00 (minimaal 30 min later)

### Account
* Aanpassen van:
  * Adres
  * (leveradres?, wachtwoord, telefoonnummer)
* Niet ingelogd -> sessie vars 
* Ingelogd -> database

----
### Andere voorzienningen en varia
#### Producten
* Naam, Prijs
* eventueel: 
  * samenstelling, voedingwaarden
  * beschikbaarheid (seizoenafhankelijk)
  * vlees, vegie, gluttenvrij, vis, ...

#### Klanten
* Wachtwoord ~~MD5~~ sha512 (VARCHAR(128) of CHAR(128))
* Bij login: emailadres in cookie
* geboortedatum (geen bestelling onder 18)
* eventueel: 
  * toon geen:
    * vlees
  * promo's:
    * 30% korting op 10<sup>de</sup> bestelling 


#### Algemeen (optineel)
Contactinfo, wie zijn wij, voorwaarden, gastenboek, review van pizza x
* optioneel
  * meerdere vestegingen 
    * met elk hun postcodes (eventueel overlappend)

#### Bestelinformatie
* klantennummer, timestamp bestelling, aantal, soorten, extra's, kostprijs
* optioneel
  * info voor koerier
  * wnr leveren

#### Alle promo's:
* (niet cumuleerbaar) 
* 20% korting op dinsdag en zaterdag
* x % op pizza y (in te stellen door adminaccount) -> wel cumuleerbaar?
* 30% korting op 10<sup>de</sup> bestelling
* 1 pizza gratis bij aankoop van x pizza's
* verschillende kortingsbonnen (veld bij ``Afrekenen``)
  * maar 1 keer te gebruiken per account
* korting pas bij voorwaarden
  * vanaf totaal van €2



------

### Aanpassingen: 
* Eventueel leveringsadres?


------ 
## Werkwijze
* ``git init``
* ``npm init`` (in ``www``)
* ``composer init``
* ``npm i gulp -D``

